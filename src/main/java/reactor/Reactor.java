package reactor;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Reactor多线程模型
 */
public class Reactor {
    public static void main(String[] args) throws IOException {
        //获取一个Selector
        Selector selector = Selector.open();
        //开启一个服务器channel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //非阻塞
        serverSocketChannel.configureBlocking(false);
        //绑定一个端口
        serverSocketChannel.bind(new InetSocketAddress(9000));
        //监听注册事件，将这个channel注册到selector上
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        while (true) {
            if (selector.selectNow() < 0) {
                continue;
            }
            //如果selectorKey集合有东西，就遍历事件
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                //如果是连接事件，就拿到通道建立连接
                if (key.isAcceptable()) {
                    ServerSocketChannel acceptServerSocketChannel = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = acceptServerSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    System.out.println("Accept request from " + socketChannel.getRemoteAddress());
                    //注册一个readyKey到selector里
                    SelectionKey readKey = socketChannel.register(selector, SelectionKey.OP_READ);
                    //新建个线程处理绑定这个key
                    readKey.attach(new Processor());
                    //处理读事件
                } else if (key.isReadable()) {
                    Processor processor = (Processor) key.attachment();
                    processor.process(key);
                }
            }
        }
    }
}

class Processor {
    private static final ExecutorService SERVICE = Executors.newFixedThreadPool(16);

    public void process(SelectionKey selectionKey) {
        //线程池提交任务
        SERVICE.submit(() -> {
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
            int count = socketChannel.read(buffer);
            if (count < 0) {
                socketChannel.close();
                selectionKey.cancel();
                System.out.println(socketChannel + "\t Read ended");
                return null;
            } else if (count == 0) {
                return null;
            }
            System.out.println(socketChannel + "\t Read message " + new String(buffer.array()));
            return null;
        });
    }
}
