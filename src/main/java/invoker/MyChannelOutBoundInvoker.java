package invoker;

import future.MyChannelFuture;
import io.netty.channel.ChannelProgressivePromise;
import promise.MyChannelPromise;

import java.net.SocketAddress;

public interface MyChannelOutBoundInvoker {

    /**
     * 绑定一个端口
     *
     * @param localAddress 端口地址
     * @return 一个channelFuture对象
     */
    MyChannelFuture bind(SocketAddress localAddress);

    MyChannelFuture connect(SocketAddress remoteAddress);

    MyChannelFuture connect(SocketAddress remoteAddress, SocketAddress localAddress);

    MyChannelFuture disconnect();

    MyChannelFuture close();

    MyChannelFuture deregister();

    MyChannelFuture bind(SocketAddress localAddress, MyChannelPromise promise);

    MyChannelFuture connect(SocketAddress remoteAddress, MyChannelPromise promise);

    MyChannelFuture connect(SocketAddress remoteAddress, SocketAddress localAddress, MyChannelPromise promise);

    MyChannelFuture disconnect(MyChannelPromise promise);

    MyChannelFuture close(MyChannelPromise promise);

    MyChannelFuture deregister(MyChannelPromise promise);

    MyChannelOutBoundInvoker read();

    MyChannelFuture write(Object msg);

    MyChannelFuture write(Object msg, MyChannelPromise promise);

    MyChannelOutBoundInvoker flush();

    MyChannelFuture writeAndFlush(Object msg, MyChannelPromise promise);

    MyChannelFuture writeAndFlush(Object msg);

    MyChannelPromise newPromise();

    ChannelProgressivePromise newProgressivePromise();

    MyChannelFuture newSucceededFuture();

    MyChannelFuture newFailedFuture(Throwable cause);

    MyChannelPromise voidPromise();
}
