package ByteBuf;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

public class Scratch {

    public static void main(String[] args) {
        PooledByteBufAllocator allocator = PooledByteBufAllocator.DEFAULT;

        ByteBuf byteBuf = allocator.directBuffer(16);

        byteBuf.release();
    }
}
