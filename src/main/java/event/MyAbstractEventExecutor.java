package event;

import future.MyFuture;
import future.MySucceedFuture;
import io.netty.util.concurrent.ScheduledFuture;
import promise.MyDefaultProgressivePromise;
import promise.MyDefaultPromise;
import promise.MyProgressivePromise;
import promise.MyPromise;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public abstract class MyAbstractEventExecutor extends AbstractExecutorService implements MyEventExecutor {


    static final long DEFAULT_SHUTDOWN_QUIET_PERIOD = 2;
    static final long DEFAULT_SHUTDOWN_TIMEOUT = 15;

    private final MyEventExecutorGroup parent;
    private final Collection<MyEventExecutor> selfCollection = Collections.singleton(this);


    public MyAbstractEventExecutor() {
        this(null);
    }

    public MyAbstractEventExecutor(MyEventExecutorGroup parent) {
        this.parent = parent;
    }


    @Override
    public boolean isShuttingDown() {
        return false;
    }

    @Override
    public MyFuture<?> shutdownGracefully() {
        return shutdownGracefully(DEFAULT_SHUTDOWN_QUIET_PERIOD, DEFAULT_SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
    }

    @Override
    public MyFuture<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit) {
    }

    @Override
    public MyFuture<?> terminationFuture() {
        return null;
    }

    @Override
    @Deprecated
    public abstract void shutdown();


    @Override
    public MyEventExecutor next() {
        return this;
    }

    @Override
    public Iterator<MyEventExecutor> iterator() {
        return selfCollection.iterator();
    }

    @Override
    public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return null;
    }


    @Override
    public MyEventExecutorGroup parent() {
        return parent;
    }

    @Override
    public boolean inEventLoop() {
        return inEventLoop(Thread.currentThread());
    }

    @Override
    public boolean inEventLoop(Thread thread) {
        return false;
    }

    @Override
    public List<Runnable> shutdownNow() {
        shutdown();
        return Collections.emptyList();
    }

    @Override
    public boolean isShutdown() {
        return false;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public <V> MyPromise<V> newPromise() {
        return new MyDefaultPromise<V>(this);
    }

    @Override
    public <V> MyProgressivePromise<V> newProgressivePromise() {
        return new MyDefaultProgressivePromise<>(this);
    }

    @Override
    public <V> MyFuture<V> newSucceededFuture(V result) {
        return new MySucceedFuture<V>(this, result);
    }

    @Override
    public <V> MyFuture<V> newFailedFuture(Throwable cause) {
        return null;
    }

    @Override
    public void execute(Runnable command) {

    }
}
