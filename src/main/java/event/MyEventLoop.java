package event;

public interface MyEventLoop extends MyOrderedEventExecutor,MyEventLoopGroup {

    @Override
    MyEventLoopGroup parent();
}
