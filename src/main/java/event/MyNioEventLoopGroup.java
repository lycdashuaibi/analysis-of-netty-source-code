package event;

import io.netty.channel.DefaultSelectStrategyFactory;
import io.netty.channel.EventLoopTaskQueueFactory;
import io.netty.channel.SelectStrategyFactory;
import io.netty.util.concurrent.EventExecutorChooserFactory;
import io.netty.util.concurrent.RejectedExecutionHandler;
import io.netty.util.concurrent.RejectedExecutionHandlers;

import java.nio.channels.spi.SelectorProvider;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;

public class MyNioEventLoopGroup extends MyMultithreadEventExecutorGroup {


    public MyNioEventLoopGroup() {
        this(0);
    }

    public MyNioEventLoopGroup(int nThreads) {
        // 将默认线程 Executor （Java Executor）置为 null。为后续使用默认 Exceutor 铺垫
        this(nThreads, (Executor) null);
    }

    public MyNioEventLoopGroup(int nThreads, Executor executor) {

        // 使用 `SelectorProvider.provider()` 提供  `SelectorProvider`， 细节下文进行描述
        this(nThreads, executor, SelectorProvider.provider());
    }

    public MyNioEventLoopGroup(ThreadFactory threadFactory) {
        this(0, threadFactory, SelectorProvider.provider());
    }

    public MyNioEventLoopGroup(int nThreads, ThreadFactory threadFactory) {
        this(nThreads, threadFactory, SelectorProvider.provider());
    }

    public MyNioEventLoopGroup(
            int nThreads, Executor executor, final SelectorProvider selectorProvider) {
        //  使用 `DefaultSelectStrategyFactory.INSTANCE` 为 NioEventLoopGroup 提供默认的 `SelectStrategyFactory`。
        this(nThreads, executor, selectorProvider, DefaultSelectStrategyFactory.INSTANCE);
    }

    public MyNioEventLoopGroup(
            int nThreads, ThreadFactory threadFactory, final SelectorProvider selectorProvider) {
        this(nThreads, threadFactory, selectorProvider, DefaultSelectStrategyFactory.INSTANCE);
    }

    public MyNioEventLoopGroup(int nThreads, ThreadFactory threadFactory,
                               final SelectorProvider selectorProvider, final SelectStrategyFactory selectStrategyFactory) {
        super(nThreads, threadFactory, selectorProvider, selectStrategyFactory, RejectedExecutionHandlers.reject());
    }

    public MyNioEventLoopGroup(int nThreads, Executor executor, final SelectorProvider selectorProvider,
                               final SelectStrategyFactory selectStrategyFactory) {

        /**
         * 以以下内容为内容调用父类 MultithreadEventLoopGroup 的构造器
         * 将上面所定义出的 nThreads, executor, selectorProvider, selectStrategyFactory
         * 以及 此处提供 `RejectedExecutionHandler`
         */
        super(nThreads, executor, selectorProvider, selectStrategyFactory, RejectedExecutionHandlers.reject());
    }

    public MyNioEventLoopGroup(int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory,
                               final SelectorProvider selectorProvider,
                               final SelectStrategyFactory selectStrategyFactory) {
        super(nThreads, executor, chooserFactory, selectorProvider, selectStrategyFactory,
                RejectedExecutionHandlers.reject());
    }

    public MyNioEventLoopGroup(int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory,
                               final SelectorProvider selectorProvider,
                               final SelectStrategyFactory selectStrategyFactory,
                               final RejectedExecutionHandler rejectedExecutionHandler) {
        super(nThreads, executor, chooserFactory, selectorProvider, selectStrategyFactory, rejectedExecutionHandler);
    }

    public MyNioEventLoopGroup(int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory,
                               final SelectorProvider selectorProvider,
                               final SelectStrategyFactory selectStrategyFactory,
                               final RejectedExecutionHandler rejectedExecutionHandler,
                               final EventLoopTaskQueueFactory taskQueueFactory) {
        super(nThreads, executor, chooserFactory, selectorProvider, selectStrategyFactory,
                rejectedExecutionHandler, taskQueueFactory);
    }


    @Override
    protected MyEventExecutor newChild(Executor executor, Object... args) throws Exception {
        return null;
    }
}
