package event;

import future.MyFuture;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;
import promise.MyProgressivePromise;
import promise.MyPromise;

/**
 * @author liangyucheng
 * 具体线程接口
 */
public interface MyEventExecutor extends MyEventExecutorGroup {

    /**
     * 下一个线程
     *
     * @return 返回下一个线程
     */
    @Override
    MyEventExecutor next();

    /**
     * 返回所属线程组
     *
     * @return
     */
    MyEventExecutorGroup parent();

    /**
     * 如果线程是在这个线程组
     *
     * @return true
     */
    boolean inEventLoop();

    /**
     * 如果线程是在这个线程组内
     *
     * @param thread 线程
     * @return true
     */
    boolean inEventLoop(Thread thread);

    /**
     * 下一个promise
     *
     * @param <V> promise又是啥
     * @return promise 异步编程任务
     */
    <V> MyPromise<V> newPromise();


    <V> MyProgressivePromise<V> newProgressivePromise();

    /**
     * Create a new {@link Future} which is marked as succeeded already. So {@link Future#isSuccess()}
     * will return {@code true}. All {@link FutureListener} added to it will be notified directly. Also
     * every call of blocking methods will just return without blocking.
     */
    <V> MyFuture<V> newSucceededFuture(V result);

    /**
     * Create a new {@link Future} which is marked as failed already. So {@link Future#isSuccess()}
     * will return {@code false}. All {@link FutureListener} added to it will be notified directly. Also
     * every call of blocking methods will just return without blocking.
     */
    <V>  MyFuture<V> newFailedFuture(Throwable cause);
}
