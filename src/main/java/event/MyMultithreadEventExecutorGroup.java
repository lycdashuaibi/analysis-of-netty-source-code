package event;

import future.MyFuture;
import io.netty.util.concurrent.*;
import promise.MyDefaultPromise;
import promise.MyPromise;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author liangyucheng
 */
public abstract class MyMultithreadEventExecutorGroup extends MyAbstractEventExecutorGroup {

    /**
     * 处理器线程组
     */
    private final MyEventExecutor[] children;

    /**
     *
     */
    private final Set<MyEventExecutor> readOnlyChildren;

    private final AtomicInteger terminatedChildren = new AtomicInteger();

    // // FIXME: 2021/7/22 修改成自己的Myomise
    private final MyPromise terminationFuture = new MyDefaultPromise(new MyGlobalEventExecutor());


    /**
     * 选择工厂
     */
    private final EventExecutorChooserFactory.EventExecutorChooser chooser;

    /**
     * 此类的调用入口
     *
     * @param nThreads      线程数
     * @param threadFactory 线程工厂
     * @param args          其他参数
     */
    public MyMultithreadEventExecutorGroup(int nThreads, ThreadFactory threadFactory, Object... args) {
        this(nThreads, threadFactory == null ? null : new ThreadPerTaskExecutor(threadFactory), args);

    }

    public MyMultithreadEventExecutorGroup(int nThreads, Executor executor, Object[] args) {
        this(nThreads, executor, DefaultEventExecutorChooserFactory.INSTANCE, args);

    }


    public MyMultithreadEventExecutorGroup(int nThreads, Executor executor, EventExecutorChooserFactory chooserFactory, Object[] args) {
        //检验传入的Thread数是否大于0
        if (nThreads <= 0) {
            throw new IllegalArgumentException(String.format("nThreads: %d (expected: > 0)", nThreads));
        }
        if (executor == null) {
            executor = new ThreadPerTaskExecutor(newDefaultThreadFactory());
        }
        //初始化children数组
        children = new MyEventExecutor[nThreads];
        for (int i = 0; i < nThreads; i++) {
            boolean success = false;
            try {
                children[i] = newChild(executor, args);
                success = true;
            } catch (Exception e) {
                throw new IllegalStateException("failed to create a child event loop", e);
            } finally {
                if (!success) {
                    //初始化children节点失败时，将children所有的EventExecutor关闭
                    for (int j = 0; j < i; j++) {
                        children[j].shutdownGracefully();
                    }
                    //确保所有的EventExecutor所有的task被执行完成后再关闭
                    for (int j = 0; j < i; j++) {
                        MyEventExecutor e = children[j];
                        try {
                            while (!e.isTerminated()) {
                                e.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
                            }
                        } catch (InterruptedException interrupted) {
                            // Let the caller handle the interruption.
                            Thread.currentThread().interrupt();
                            break;
                        }
                    }
                }
            }
        }
        //// TODO: 2021/7/22  写自己的ChooserFactory
        chooser = chooserFactory.newChooser(children);

        final FutureListener<Object> terminationListener = future -> {
            if (terminatedChildren.incrementAndGet() == children.length) {
                terminationFuture.setSuccess(null);
            }
        };
        /**
         * 添加Listener
         */
        for (EventExecutor e : children) {
            e.terminationFuture().addListener(terminationListener);
        }
        Set<MyEventExecutor> childrenSet = new LinkedHashSet<>(children.length);
        Collections.addAll(childrenSet, children);
        readOnlyChildren = Collections.unmodifiableSet(childrenSet);
    }


    protected ThreadFactory newDefaultThreadFactory() {
        return new DefaultThreadFactory(getClass());
    }

    @Override
    public MyEventExecutor next() {
        return chooser.next();
    }

    @Override
    public Iterator<MyEventExecutor> iterator() {
        return readOnlyChildren.iterator();
    }

    /**
     * Return the number of {@link EventExecutor} this implementation uses. This number is the maps
     * 1:1 to the threads it use.
     */
    public final int executorCount() {
        return children.length;
    }

    /**
     * Create a new EventExecutor which will later then accessible via the {@link #next()}  method. This method will be
     * called for each thread that will serve this {@link MultithreadEventExecutorGroup}.
     */
    protected abstract MyEventExecutor newChild(Executor executor, Object... args) throws Exception;

    @Override
    public MyFuture<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit) {
        for (MyEventExecutor l : children) {
            l.shutdownGracefully(quietPeriod, timeout, unit);
        }
        return terminationFuture();
    }

    @Override
    public MyFuture<?> terminationFuture() {
        return terminationFuture;
    }

    @Override
    @Deprecated
    public void shutdown() {
        for (MyEventExecutor l : children) {
            l.shutdown();
        }
    }

    @Override
    public boolean isShuttingDown() {
        for (MyEventExecutor l : children) {
            if (!l.isShuttingDown()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isShutdown() {
        for (MyEventExecutor l : children) {
            if (!l.isShutdown()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isTerminated() {
        for (MyEventExecutor l : children) {
            if (!l.isTerminated()) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param timeout
     * @param unit
     * @return
     * @throws InterruptedException
     */
    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit)
            throws InterruptedException {
        long deadline = System.nanoTime() + unit.toNanos(timeout);

        loop:
        for (MyEventExecutor l : children) {
            for (; ; ) {
                long timeLeft = deadline - System.nanoTime();
                if (timeLeft <= 0) {
                    break loop;
                }

                if (l.awaitTermination(timeLeft, TimeUnit.NANOSECONDS)) {
                    break;
                }
            }
        }
        return isTerminated();
    }
}

