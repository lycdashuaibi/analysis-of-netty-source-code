package event;

import future.MyFuture;
import io.netty.util.concurrent.ScheduledFuture;
import promise.MyProgressivePromise;
import promise.MyPromise;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class MyGlobalEventExecutor implements MyOrderedEventExecutor {
    @Override
    public boolean isShuttingDown() {
        return false;
    }

    @Override
    public MyFuture<?> shutdownGracefully() {
        return null;
    }

    @Override
    public MyFuture<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit) {
        return null;
    }

    @Override
    public MyFuture<?> terminationFuture() {
        return null;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public List<Runnable> shutdownNow() {
        return null;
    }

    @Override
    public boolean isShutdown() {
        return false;
    }

    @Override
    public boolean isTerminated() {
        return false;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public MyEventExecutor next() {
        return null;
    }

    @Override
    public Iterator<MyEventExecutor> iterator() {
        return null;
    }

    @Override
    public MyFuture<?> submit(Runnable task) {
        return null;
    }

    @Override
    public <T> List<java.util.concurrent.Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        return null;
    }

    @Override
    public <T> List<java.util.concurrent.Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        return null;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        return null;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return null;
    }

    @Override
    public <T> MyFuture<T> submit(Runnable task, T result) {
        return null;
    }

    @Override
    public <T> MyFuture<T> submit(Callable<T> task) {
        return null;
    }

    @Override
    public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return null;
    }

    @Override
    public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return null;
    }

    @Override
    public MyEventExecutorGroup parent() {
        return null;
    }

    @Override
    public boolean inEventLoop() {
        return false;
    }

    @Override
    public boolean inEventLoop(Thread thread) {
        return false;
    }

    @Override
    public <V> MyPromise<V> newPromise() {
        return null;
    }

    @Override
    public <V> MyProgressivePromise<V> newProgressivePromise() {
        return null;
    }

    @Override
    public <V> MyFuture<V> newSucceededFuture(V result) {
        return null;
    }

    @Override
    public <V> MyFuture<V> newFailedFuture(Throwable cause) {
        return null;
    }

    @Override
    public void execute(Runnable command) {

    }
}
