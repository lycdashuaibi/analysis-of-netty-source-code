package event;

import io.netty.util.concurrent.ScheduledFuture;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author liangyucheng
 * 最上层Nio线程执行器组
 */
public interface MyEventExecutorGroup extends ScheduledExecutorService, Iterable<MyEventExecutor> {
    /**
     * 所有的Nio线程都被关掉，返回true
     * 或者被shutdown
     *
     * @return true or false
     */
    boolean isShuttingDown();

    /**
     * 关掉线程组
     *
     * @return the {@link #terminationFuture()}
     */
    Future<?> shutdownGracefully();

    /**
     * 调用后 {@link #isShuttingDown();} 会返回true
     *
     * @param quietPeriod 通知其他线程，执行完最后的任务
     * @param timeout     最大等待时间直到线程关闭
     * @param unit        几秒后关闭
     * @return
     */
    Future<?> shutdownGracefully(long quietPeriod, long timeout, TimeUnit unit);

    /**
     * @return 暂时还不清楚
     */
    Future<?> terminationFuture();

    /**
     * 暂时不使用了
     *
     * @return 返回所有线程
     * @deprecated {@link #shutdownGracefully(long, long, TimeUnit)} or {@link #shutdownGracefully()} instead.
     */
    @Override
    @Deprecated
    List<Runnable> shutdownNow();

    /**
     * 返回下一个线程
     *
     * @return 返回下一个线程
     */
    MyEventExecutor next();

    /**
     * 线程迭代器
     *
     * @return iterator
     */
    @Override
    Iterator<MyEventExecutor> iterator();

    /**
     * 提交一个异步任务
     *
     * @param task
     * @return future
     */
    @Override
    Future<?> submit(Runnable task);

    @Override
    <T> Future<T> submit(Runnable task, T result);

    @Override
    <T> Future<T> submit(Callable<T> task);

    /**
     * 提交定时任务
     *
     * @param command
     * @param delay
     * @param unit
     * @return
     */
    @Override
    ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit);

    @Override
    <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit);

    @Override
    ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit);

    @Override
    ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit);
}
