package promise;

import event.MyEventExecutor;
import future.MyAbstractFuture;
import future.MyFuture;
import future.futurelistener.MyDefaultFutureListeners;
import future.futurelistener.MyGenericFutureListener;
import io.netty.util.concurrent.BlockingOperationException;
import io.netty.util.internal.InternalThreadLocalMap;
import io.netty.util.internal.SystemPropertyUtil;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public class MyDefaultPromise<V> extends MyAbstractFuture<V> implements MyPromise<V> {

    /**
     * 不能取消事件？
     */
    private static final Object UNCANCELLABLE = new Object();
    private static final Object SUCCESS = new Object();
    private static final AtomicReferenceFieldUpdater<MyDefaultPromise, Object> RESULT_UPDATER =
            AtomicReferenceFieldUpdater.newUpdater(MyDefaultPromise.class, Object.class, "result");
    private static final int MAX_LISTENER_STACK_DEPTH = Math.min(8,
            SystemPropertyUtil.getInt("io.netty.defaultPromise.maxListenerStackDepth", 8));
    private final MyEventExecutor executor;
    /**
     * io运行的结果
     */
    private volatile Object result;
    private boolean notifyingListeners;
    private Object listeners;
    private short waiters;

    public MyDefaultPromise() {
        executor = null;
    }

    public MyDefaultPromise(MyEventExecutor executor) {
        if (executor == null) {
            throw new NullPointerException();
        }
        this.executor = executor;
    }

    private static void notifyListener0(MyFuture future, MyGenericFutureListener l) {
        try {
            l.operationComplete(future);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    private static void safeExecute(MyEventExecutor executor, Runnable task) {
        try {
            executor.execute(task);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public static void NotifyListener(MyEventExecutor eventExecutor, final MyFuture<?> future, final MyGenericFutureListener<?> listener) {
        notifyListenerWithStackOverFlowProtection(eventExecutor, future, listener);
    }

    private static void notifyListenerWithStackOverFlowProtection(final MyEventExecutor executor,
                                                                  final MyFuture<?> future,
                                                                  final MyGenericFutureListener<?> listener) {
    }


    @Override
    public boolean isSuccess() {
        return false;
    }

    @Override
    public boolean isCancellable() {
        return false;
    }

    @Override
    public Throwable cause() {
        return null;
    }

    @Override
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        return false;
    }

    @Override
    public boolean await(long timeoutMillis) throws InterruptedException {
        return false;
    }

    @Override
    public boolean awaitUninterruptibly(long timeout, TimeUnit unit) {
        return false;
    }

    @Override
    public boolean awaitUninterruptibly(long timeoutMillis) {
        return false;
    }

    @Override
    public V getNow() {
        return null;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return false;
    }

    @Override
    public MyPromise<V> setSuccess(V result) {
        /*
        如果能设置成功状态，则返回本身
        否则就抛出异常
         */
        if (setSuccess0(result)) {
            return this;
        }
        throw new IllegalArgumentException("complete already: " + this);
    }

    @Override
    public boolean trySuccess(V result) {
        return false;
    }

    @Override
    public MyPromise<V> setFailure(Throwable cause) {
        return null;
    }

    @Override
    public boolean tryFailure(Throwable cause) {
        return false;
    }

    @Override
    public MyPromise<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener) {
        return null;
    }

    @Override
    public MyPromise<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners) {
        return null;
    }

    @Override
    public MyPromise<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners) {
        return null;
    }

    @Override
    public MyPromise<V> sync() throws InterruptedException {
        return null;
    }

    @Override
    public MyPromise<V> syncUninterruptibly() {
        return null;
    }

    @Override
    public MyPromise<V> await() throws InterruptedException {
        if (isDone()) {
            return this;
        }

        if (Thread.interrupted()) {
            throw new InterruptedException(toString());
        }

        checkDeadLock();

        synchronized (this) {
            while (!isDone()) {
                incWaiters();
                try {
                    wait();
                } finally {
                    decWaiters();
                }
            }
        }
        return this;
    }

    private void incWaiters() {
        if (waiters == Short.MAX_VALUE) {
            throw new IllegalStateException("too many waiters: " + this);
        }
        ++waiters;
    }

    private void decWaiters() {
        --waiters;
    }

    private void checkDeadLock() {
        MyEventExecutor e = executor();
        if (e != null && e.inEventLoop()) {
            throw new BlockingOperationException(toString());
        }
    }

    @Override
    public MyPromise<V> awaitUninterruptibly() {
        return null;
    }

    public boolean setSuccess0(V result) {
        return setValue0(result == null ? SUCCESS : result);
    }

    private boolean setValue0(Object objResult) {
        //cas更新变量
        if (RESULT_UPDATER.compareAndSet(this, null, objResult) ||
                RESULT_UPDATER.compareAndSet(this, UNCANCELLABLE, objResult)) {
            if (checkNotifyWaiters()) {
                notifyListeners();
            }
            return true;
        }
        return false;

    }

    private synchronized boolean checkNotifyWaiters() {
        //如果有等待的线程，就全部唤醒
        //避免并发操作
        if (waiters > 0) {
            notifyAll();
        }
        //监听器不等于空
        return listeners != null;
    }

    private void notifyListeners() {
        MyEventExecutor executor = executor();
        if (executor.inEventLoop()) {
            //TODO: 更换自己的InternalThreadLocalMap
            final InternalThreadLocalMap threadLocals = InternalThreadLocalMap.get();
            final int stackDepth = threadLocals.futureListenerStackDepth();
            if (stackDepth < MAX_LISTENER_STACK_DEPTH) {
                threadLocals.setFutureListenerStackDepth(stackDepth + 1);
                try {
                    notifyListenersNow();
                } finally {
                    threadLocals.setFutureListenerStackDepth(stackDepth);
                }
                return;
            }
        }


        safeExecute(executor, (Runnable) () -> notifyListenersNow());
    }

    protected MyEventExecutor executor() {
        return executor;
    }

    private void notifyListenersNow() {
        Object listeners;
        synchronized (this) {
            // Only proceed if there are listeners to notify and we are not already notifying listeners.
            if (notifyingListeners || this.listeners == null) {
                return;
            }
            notifyingListeners = true;
            listeners = this.listeners;
            this.listeners = null;
        }
        for (; ; ) {
            if (listeners instanceof MyDefaultFutureListeners) {
                notifyListeners0((MyDefaultFutureListeners) listeners);
            } else {
                notifyListener0(this, (MyGenericFutureListener<?>) listeners);
            }
            synchronized (this) {
                if (this.listeners == null) {
                    // Nothing can throw from within this method, so setting notifyingListeners back to false does not
                    // need to be in a finally block.
                    notifyingListeners = false;
                    return;
                }
                listeners = this.listeners;
                this.listeners = null;
            }
        }
    }

    private void notifyListeners0(MyDefaultFutureListeners listeners) {
        MyGenericFutureListener<?>[] a = listeners.listeners();
        int size = listeners.size();
        for (int i = 0; i < size; i++) {
            notifyListener0(this, a[i]);
        }
    }

    private static final class CauseHolder {
        final Throwable cause;

        CauseHolder(Throwable cause) {
            this.cause = cause;
        }
    }
}
