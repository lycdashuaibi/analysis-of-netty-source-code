package promise;

import future.MyFuture;
import future.futurelistener.MyGenericFutureListener;

/**
 * @author liangyucheng
 */
public interface MyPromise<V> extends MyFuture<V> {


    /**
     * 标记这个future完成了，然后提醒其他监听器
     *
     * @param result 结果事件
     * @return 返回这个Promise
     */
    MyPromise<V> setSuccess(V result);

    /**
     * 尝试判断future是否完成
     *
     * @param result 结果时间
     * @return true or false
     */
    boolean trySuccess(V result);

    /**
     * 设置future为失败，然后通知其他失败
     *
     * @param cause 失败的异常原因
     * @return 本身Promise
     */
    MyPromise<V> setFailure(Throwable cause);

    /**
     * 尝试判断future是否失败
     *
     * @param cause 原因
     * @return true or false
     */
    boolean tryFailure(Throwable cause);

    @Override
    MyPromise<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener);

    @Override
    MyPromise<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    @Override
    MyPromise<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    @Override
    MyPromise<V> sync() throws InterruptedException;

    @Override
    MyPromise<V> syncUninterruptibly();

    @Override
    MyPromise<V> await() throws InterruptedException;

    @Override
    MyPromise<V> awaitUninterruptibly();
}
