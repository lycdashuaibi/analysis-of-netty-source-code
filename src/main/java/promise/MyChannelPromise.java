package promise;

import channel.MyChannel;
import future.MyChannelFuture;
import future.MyFuture;
import future.futurelistener.MyGenericFutureListener;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public interface MyChannelPromise extends MyChannelFuture, MyPromise<Void> {

    @Override
    MyChannel channel();

    @Override
    boolean isSuccess();

    @Override
    boolean isCancellable();

    @Override
    Throwable cause();

    @Override
    MyPromise<Void> setSuccess(Void result);

    @Override
    boolean trySuccess(Void result);

    @Override
    MyPromise<Void> setFailure(Throwable cause);

    @Override
    boolean tryFailure(Throwable cause);

    @Override
    MyChannelPromise addListener(MyGenericFutureListener<? extends MyFuture<? super Void>> listener);

    @Override
    MyChannelPromise addListeners(MyGenericFutureListener<? extends MyFuture<? super Void>>... listeners);

    @Override
    MyPromise<Void> removeListeners(MyGenericFutureListener<? extends MyFuture<? super Void>>... listeners);

    @Override
    MyChannelPromise await() throws InterruptedException;

    @Override
    MyChannelPromise awaitUninterruptibly();

    @Override
    boolean await(long timeout, TimeUnit unit) throws InterruptedException;

    @Override
    boolean await(long timeoutMillis) throws InterruptedException;

    @Override
    boolean awaitUninterruptibly(long timeout, TimeUnit unit);

    @Override
    boolean awaitUninterruptibly(long timeoutMillis);

    @Override
    Void getNow();

    @Override
    boolean cancel(boolean mayInterruptIfRunning);

    @Override
    boolean isCancelled();

    @Override
    boolean isDone();

    @Override
    Void get() throws InterruptedException, ExecutionException;

    @Override
    Void get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException;

    @Override
    MyChannelPromise sync() throws InterruptedException;

    @Override
    MyChannelPromise syncUninterruptibly();

    @Override
    boolean isVoid();
}
