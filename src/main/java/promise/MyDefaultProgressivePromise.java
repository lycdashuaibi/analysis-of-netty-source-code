package promise;

import event.MyEventExecutor;
import future.MyFuture;
import future.futurelistener.MyGenericFutureListener;

public class MyDefaultProgressivePromise<V> extends MyDefaultPromise<V> implements MyProgressivePromise<V> {

    public MyDefaultProgressivePromise(MyEventExecutor executor) {
        super(executor);
    }

    public MyDefaultProgressivePromise() {
        super();
    }


    @Override
    public MyProgressivePromise<V> setProgress(long progress, long total) {
        return null;
    }

    @Override
    public boolean tryProgress(long progress, long total) {
        return false;
    }

    @Override
    public MyProgressivePromise<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener) {
        super.addListener(listener);
        return this;
    }

    @Override
    public MyProgressivePromise<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners) {
        super.addListeners(listeners);
        return this;
    }


    @Override
    public MyProgressivePromise<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners) {
        super.removeListeners(listeners);
        return this;
    }

    @Override
    public MyProgressivePromise<V> sync() throws InterruptedException {
        super.sync();
        return this;
    }

    @Override
    public MyProgressivePromise<V> syncUninterruptibly() {
        super.syncUninterruptibly();
        return this;
    }

    @Override
    public MyProgressivePromise<V> await() throws InterruptedException {
        super.await();
        return this;
    }

    @Override
    public MyProgressivePromise<V> awaitUninterruptibly() {
        super.awaitUninterruptibly();
        return this;
    }

    @Override
    public MyProgressivePromise<V> setSuccess(V result) {
        super.setSuccess(result);
        return this;
    }

    @Override
    public MyProgressivePromise<V> setFailure(Throwable cause) {
        super.setFailure(cause);
        return this;
    }
}
