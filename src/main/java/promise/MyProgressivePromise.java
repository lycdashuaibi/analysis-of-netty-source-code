package promise;

import future.MyFuture;
import future.MyProgressiveFuture;
import future.futurelistener.MyGenericFutureListener;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public interface MyProgressivePromise<V> extends MyPromise<V>, MyProgressiveFuture<V> {
    @Override
    boolean isSuccess();

    @Override
    boolean isCancellable();

    @Override
    Throwable cause();

    @Override
    boolean await(long timeout, TimeUnit unit) throws InterruptedException;

    @Override
    boolean await(long timeoutMillis) throws InterruptedException;

    @Override
    boolean awaitUninterruptibly(long timeout, TimeUnit unit);

    @Override
    boolean awaitUninterruptibly(long timeoutMillis);

    @Override
    V getNow();

    @Override
    boolean cancel(boolean mayInterruptIfRunning);

    @Override
    boolean isCancelled();

    @Override
    boolean isDone();

    @Override
    V get() throws InterruptedException, ExecutionException;

    @Override
    V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException;

    @Override
    MyPromise<V> setSuccess(V result);

    @Override
    boolean trySuccess(V result);

    @Override
    MyPromise<V> setFailure(Throwable cause);

    @Override
    boolean tryFailure(Throwable cause);

    @Override
    MyProgressivePromise<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener);

    @Override
    MyProgressivePromise<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    @Override
    MyProgressivePromise<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    @Override
    MyProgressivePromise<V> sync() throws InterruptedException;

    @Override
    MyProgressivePromise<V> syncUninterruptibly();

    @Override
    MyProgressivePromise<V> await() throws InterruptedException;

    @Override
    MyProgressivePromise<V> awaitUninterruptibly();

    // 完成后通知listener
    MyProgressivePromise<V> setProgress(long progress, long total);

    //如果操作完成或者进程超除范围
    boolean tryProgress(long progress, long total);
}
