package util;

import io.netty.util.concurrent.FastThreadLocal;

public class FastThreadLocalTest {

    //static  Object  object = new Object();

    private static FastThreadLocal<Object> threadLocal = new FastThreadLocal<Object>() {
        @Override
        protected Object initialValue() {
            return new Object();
        }
    };

    public static void main(String[] args) {
        new Thread(() -> {
            Object obj = threadLocal.get();
            // do with object
            System.out.println(obj);
            while (true) {
                threadLocal.set(new Object());
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        new Thread(() -> {
            Object obj = threadLocal.get();
            // do with object
            System.out.println(obj);
            while (true) {
                System.out.println(threadLocal.get() == obj);
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
