package channel.attribute;

public interface MyAttributeMap {

    <T> MyAttribute<T> attr(MyAttributeKey<T> key);

    <T> boolean hasAttr(MyAttributeKey<T> key);
}
