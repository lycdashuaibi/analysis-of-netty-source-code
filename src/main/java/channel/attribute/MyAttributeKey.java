package channel.attribute;

import io.netty.util.AbstractConstant;
import io.netty.util.ConstantPool;

public final class MyAttributeKey<T> extends AbstractConstant<MyAttributeKey<T>> {

    private static final ConstantPool<MyAttributeKey<Object>> pool = new ConstantPool<MyAttributeKey<Object>>() {
        @Override
        protected MyAttributeKey<Object> newConstant(int id, String name) {
            return new MyAttributeKey<>(id, name);
        }
    };

    /**
     * Creates a new instance.
     *
     * @param id
     * @param name
     */
    MyAttributeKey(int id, String name) {
        super(id, name);
    }

    //构建一个单例
    @SuppressWarnings("unchecked")
    public static <T> MyAttributeKey<T> valueOf(String name) {
        return (MyAttributeKey<T>) pool.valueOf(name);
    }

    public static boolean exists(String name) {
        return pool.exists(name);
    }

    @SuppressWarnings("unchecked")
    public static <T> MyAttributeKey<T> newInstance(String name) {
        return (MyAttributeKey<T>) pool.newInstance(name);
    }

    @SuppressWarnings("unchecked")
    public static <T> MyAttributeKey<T> valueOf(Class<?> firstNameComponent, String secondNameComponent) {
        return (MyAttributeKey<T>) pool.valueOf(firstNameComponent, secondNameComponent);
    }
}
