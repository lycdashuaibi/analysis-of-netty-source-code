package channel.attribute;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;

public class MyDefaultAttributeMap implements MyAttributeMap {
    //对某个类中,被volatile修饰的字段进行原子更新
    private static final AtomicReferenceFieldUpdater<MyDefaultAttributeMap, MyDefaultAttribute[]> ATTRIBUTES_UPDATER =
            AtomicReferenceFieldUpdater.newUpdater(MyDefaultAttributeMap.class, MyDefaultAttribute[].class, "attributes");

    private static final MyDefaultAttribute[] EMPTY_ATTRIBUTES = new MyDefaultAttribute[0];
    private volatile MyDefaultAttribute[] attributes = EMPTY_ATTRIBUTES;


    //二分法从数组中找到key在数组中下标的位置
    private static int serachAttributeByKey(MyDefaultAttribute[] sortedAttributes, MyAttributeKey<?> key) {
        int low = 0;
        int high = sortedAttributes.length - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            MyDefaultAttribute midVal = sortedAttributes[mid];
            MyAttributeKey midValKey = midVal.key();
            if (midValKey == key) {
                return mid;
            }
            int midValKeyId = midValKey.id();
            int keyId = key.id();
            assert midValKeyId != keyId;
            boolean searchRight = midValKeyId < keyId;
            if (searchRight) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -(low + 1);
    }

    //插入一个 使数组仍然有序
    private static void orderedCopyOnInsert(MyDefaultAttribute[] sortedSrc, int srcLength, MyDefaultAttribute[] copy,
                                            MyDefaultAttribute toInsert) {
        // let's walk backward, because as a rule of thumb, toInsert.key.id() tends to be higher for new keys
        final int id = toInsert.key().id();
        int i;
        for (i = srcLength - 1; i >= 0; i--) {
            MyDefaultAttribute attribute = sortedSrc[i];
            assert attribute.key().id() != id;
            if (attribute.key().id() < id) {
                break;
            }
            copy[i + 1] = sortedSrc[i];
        }
        copy[i + 1] = toInsert;
        final int toCopy = i + 1;
        if (toCopy > 0) {
            System.arraycopy(sortedSrc, 0, copy, 0, toCopy);
        }
    }

    //插入了中间,那后面的元素怎么办?
    @Override
    public <T> MyAttribute<T> attr(MyAttributeKey<T> key) {
        MyDefaultAttribute newAttribute = null;
        while (true) {
            MyDefaultAttribute[] attributes = this.attributes;
            int index = serachAttributeByKey(attributes, key);
            MyDefaultAttribute[] newAttributes;
            if (index >= 0) {
                MyDefaultAttribute attribute = attributes[index];
                if (!attribute.isRemoved()) {
                    return attribute;
                }
                if (newAttribute == null) {
                    newAttribute = new MyDefaultAttribute(this, key);
                }
                int count = attributes.length;
                newAttributes = Arrays.copyOf(attributes, count);
                newAttributes[index] = newAttribute;
            } else {
                if (newAttribute == null) {
                    newAttribute = new MyDefaultAttributeMap.MyDefaultAttribute<>(this, key);
                }
                final int count = attributes.length;
                newAttributes = new MyDefaultAttribute[count + 1];
                orderedCopyOnInsert(attributes, count, newAttributes, newAttribute);
            }
            if (ATTRIBUTES_UPDATER.compareAndSet(this, attributes, newAttributes)) {
                return newAttribute;
            }
        }
    }

    @Override
    public <T> boolean hasAttr(MyAttributeKey<T> key) {
        return serachAttributeByKey(attributes, key) >= 0;
    }

    //删除元素如果匹配
    //先根据key查找坐标,再分别复制两边到新数组,然后cas更新数组。
    private <T> void removeAttributeIfMatch(MyAttributeKey<T> key, MyDefaultAttributeMap.MyDefaultAttribute<T> value) {
        while (true) {
            MyDefaultAttribute[] attributes = this.attributes;
            int index = serachAttributeByKey(attributes, key);
            if (index < 0) {
                return;
            }
            MyDefaultAttribute attribute = attributes[index];
            if (attribute != value) {
                return;
            }
            int count = attributes.length;
            int newCount = count - 1;
            MyDefaultAttribute[] newAttributes = newCount == 0 ? EMPTY_ATTRIBUTES : new MyDefaultAttribute[newCount];
            System.arraycopy(attribute, 0, newAttributes, 0, index);
            int remain = count - 1 - index;
            if (remain > 0) {
                System.arraycopy(attribute, index + 1, newAttributes, index, remain);
            }
            if (ATTRIBUTES_UPDATER.compareAndSet(this, attributes, newAttributes)) {
                return;
            }
        }
    }

    private static final class MyDefaultAttribute<T> extends AtomicReference<T> implements MyAttribute<T> {

        private static final long serialVersionUID = 4618134439190501308L;
        private static final AtomicReferenceFieldUpdater<MyDefaultAttribute, MyDefaultAttributeMap> MAP_UPDATER =
                AtomicReferenceFieldUpdater.newUpdater(MyDefaultAttribute.class,
                        MyDefaultAttributeMap.class, "attributeMap");
        private final MyAttributeKey<T> key;
        private volatile MyDefaultAttributeMap attributeMap;

        MyDefaultAttribute(MyDefaultAttributeMap attributeMap, MyAttributeKey<T> key) {
            this.attributeMap = attributeMap;
            this.key = key;
        }

        @Override
        public MyAttributeKey<T> key() {
            return key;
        }

        @Override
        public T setIfAbsent(T value) {
            while (!compareAndSet(null, value)) {
                T old = get();
                if (old != null) {
                    return old;
                }
            }
            return null;
        }

        @Override
        public T getAndRemove() {
            MyDefaultAttributeMap attributeMap = this.attributeMap;
            boolean removed = attributeMap != null && MAP_UPDATER.compareAndSet(this, attributeMap, null);
            T oldValue = getAndSet(null);
            if (removed) {
                attributeMap.removeAttributeIfMatch(key, this);
            }

            return oldValue;
        }

        @Override
        public void remove() {
            final MyDefaultAttributeMap attributeMap = this.attributeMap;
            final boolean removed = attributeMap != null && MAP_UPDATER.compareAndSet(this, attributeMap, null);
            set(null);
            if (removed) {
                attributeMap.removeAttributeIfMatch(key, this);
            }
        }

        private boolean isRemoved() {
            return attributeMap == null;
        }
    }
}
