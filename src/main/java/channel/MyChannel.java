package channel;

import buffer.MyByteBufAllocator;
import future.MyChannelFuture;
import invoker.MyChannelOutBoundInvoker;
import io.netty.util.AttributeMap;

import java.net.SocketAddress;

public interface MyChannel extends Comparable<MyChannel>, AttributeMap, MyChannelOutBoundInvoker {
    //// TODO: 2021/7/27 重写出站 ChannelOutboundInvoker

    Unsafe unsafe();

    //返回全局唯一通道id
    MyChannelId id();

    //返回通道注册到的事件集合
    MyEventLoop eventLoop();

    //父通道
    //对于服务端Channel来讲，它的父channel是空，而客户端的channel，它的父channel就是创建它的ServerSocketChannel.
    MyChannel parent();

    //通道一些配置
    MyChannelConfig config();

    //是否开启通道
    boolean isOpen();

    boolean isRegistered();

    boolean isActive();

    MyChannelMetadata metadata();

    SocketAddress localaddress();

    SocketAddress remoteAddress();

    MyChannelFuture closeFuture();

    boolean isWritable();

    //返回这个通道还有多少能写入
    long bytesBeforeUnwritable();

    MyChannelPipeline pipeline();

    MyByteBufAllocator alloc();

    @Override
    MyChannel read();

    @Override
    MyChannel flush();

    //一些内部不安全操作类
    interface Unsafe {

    }
}
