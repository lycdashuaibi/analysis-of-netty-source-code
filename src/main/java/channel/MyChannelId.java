package channel;

import java.io.Serializable;

public interface MyChannelId extends Serializable, Comparable<MyChannelId> {

    String asShortText();

    String asLongText();
}
