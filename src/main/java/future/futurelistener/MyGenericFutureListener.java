package future.futurelistener;

import future.MyFuture;

import java.util.EventListener;

public interface MyGenericFutureListener<F extends MyFuture<?>> extends EventListener {


    /**
     * 异步任务完成后的回调函数
     *
     * @param future 传入一个netty子类的future
     * @throws Exception 异步任务的错误
     */
    void operationComplete(F future) throws Exception;
}
