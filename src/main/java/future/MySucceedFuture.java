package future;

import event.MyEventExecutor;

public final class MySucceedFuture<V> extends MyCompleteFuture<V> {

    private final V result;

    public MySucceedFuture(MyEventExecutor executor, V result) {
        super(executor);
        this.result = result;
    }


    @Override
    public boolean isSuccess() {
        return true;
    }

    @Override
    public Throwable cause() {
        return null;
    }

    @Override
    public V getNow() {
        return result;
    }
}
