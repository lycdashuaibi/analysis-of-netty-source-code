package future;

import channel.MyChannel;
import future.futurelistener.MyGenericFutureListener;

/**
 * @author liangyucheng
 */
public interface MyChannelFuture extends MyFuture<Void> {

    MyChannel channel();

    @Override
    MyChannelFuture addListener(MyGenericFutureListener<? extends MyFuture<? super Void>> listener);

    @Override
    MyChannelFuture addListeners(MyGenericFutureListener<? extends MyFuture<? super Void>>... listeners);

    @Override
    MyChannelFuture await() throws InterruptedException;

    @Override
    MyChannelFuture awaitUninterruptibly();

    @Override
    MyChannelFuture sync() throws InterruptedException;

    @Override
    MyChannelFuture syncUninterruptibly();

    /**
     * 返回这个future是否任务为空
     *
     * @return true or false
     */
    boolean isVoid();
}
