package future;

import event.MyEventExecutor;
import future.futurelistener.MyGenericFutureListener;
import promise.MyDefaultPromise;

import java.util.concurrent.TimeUnit;

//代表一个完成的future
public abstract class MyCompleteFuture<V> extends MyAbstractFuture<V> {
    private final MyEventExecutor executor;

    public MyCompleteFuture(MyEventExecutor executor) {
        this.executor = executor;
    }

    public MyEventExecutor executor() {
        return executor;
    }

    @Override
    public MyFuture<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener) {
        MyDefaultPromise.NotifyListener(executor(), this, listener);
        return this;
    }

    @Override
    public MyFuture<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners) {
        for (MyGenericFutureListener<? extends MyFuture<? super V>> listener : listeners) {
            if (listener == null) {
                break;
            }
            MyDefaultPromise.NotifyListener(executor(), this, listener);
        }
        return this;
    }

    @Override
    public MyFuture<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners) {
        return this;
    }

    @Override
    public MyFuture<V> await() throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        return this;
    }

    @Override
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        return true;
    }

    @Override
    public MyFuture<V> awaitUninterruptibly() {
        return this;
    }

    @Override
    public boolean await(long timeoutMillis) throws InterruptedException {
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        return true;
    }

    @Override
    public boolean awaitUninterruptibly(long timeout, TimeUnit unit) {
        if (Thread.interrupted()) {
            return false;
        }
        return true;
    }

    @Override
    public MyFuture<V> sync() throws InterruptedException {
        return this;
    }

    @Override
    public MyFuture<V> syncUninterruptibly() {
        return this;
    }

    @Override
    public boolean awaitUninterruptibly(long timeoutMillis) {
        return true;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDone() {
        return true;
    }

    @Override
    public boolean isCancellable() {
        return false;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        return false;
    }
}
