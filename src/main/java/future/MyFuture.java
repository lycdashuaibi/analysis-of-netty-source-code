package future;

import future.futurelistener.MyGenericFutureListener;

import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * @author liangyucheng、
 * Netty强化的Future类
 */
public interface MyFuture<V> extends Future<V> {

    /**
     * 异步任务是否成功结束
     *
     * @return true or false
     */
    boolean isSuccess();

    /**
     * 异步任务是否被取消
     *
     * @return true or false
     */
    boolean isCancellable();

    /**
     * 异步任务中止的原因
     *
     * @return exception
     */
    Throwable cause();

    /**
     * 给future 添加一个listener
     *
     * @param listener 事件监听器
     * @return 本身
     */
    MyFuture<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener);

    /**
     * 添加一组listener
     *
     * @param listeners 事件监听器
     * @return 本身
     */
    MyFuture<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    /**
     * 删除一组监听器
     *
     * @param listeners 事件监听器
     * @return 本身
     */
    MyFuture<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);


    /**
     * 监控future任务做完
     *
     * @return 本身
     * @throws InterruptedException 中断异常
     */
    MyFuture<V> sync() throws InterruptedException;

    /**
     * 一直监控异步任务
     *
     * @return 本身
     */
    MyFuture<V> syncUninterruptibly();

    /**
     * 等待异步任务做完
     *
     * @return 本身
     * @throws InterruptedException 中断异常
     */
    MyFuture<V> await() throws InterruptedException;

    /**
     * 等待直到发生异常
     *
     * @return 本身
     */
    MyFuture<V> awaitUninterruptibly();

    /**
     * 等待一定的事件
     *
     * @param timeout 最大超时时间
     * @param unit    时间单位
     * @return true or false
     * @throws InterruptedException 中断异常
     */
    boolean await(long timeout, TimeUnit unit) throws InterruptedException;

    /**
     * 等待事件多少秒
     *
     * @param timeoutMillis 毫秒
     * @return true or false
     * @throws InterruptedException 中断异常
     */
    boolean await(long timeoutMillis) throws InterruptedException;

    /**
     * 等待事件直到发生中断噶
     *
     * @param timeout 最大超时时间
     * @param unit    时间单位
     * @return true or false
     */
    boolean awaitUninterruptibly(long timeout, TimeUnit unit);

    boolean awaitUninterruptibly(long timeoutMillis);

    /**
     * 非阻塞获取结果的办法
     *
     * @return 获取结果
     */
    V getNow();

    /**
     * 取消任务
     *
     * @param mayInterruptIfRunning 是否取消
     * @return ture or false
     */
    @Override
    boolean cancel(boolean mayInterruptIfRunning);
}
