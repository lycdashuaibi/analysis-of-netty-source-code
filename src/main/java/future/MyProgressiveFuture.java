package future;

import future.futurelistener.MyGenericFutureListener;

/**
 * 一个用来指示操作进度的future
 *
 * @param <V>
 */
public interface MyProgressiveFuture<V> extends MyFuture<V> {

    @Override
    MyProgressiveFuture<V> addListener(MyGenericFutureListener<? extends MyFuture<? super V>> listener);

    @Override
    MyProgressiveFuture<V> addListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    @Override
    MyProgressiveFuture<V> removeListeners(MyGenericFutureListener<? extends MyFuture<? super V>>... listeners);

    @Override
    MyProgressiveFuture<V> sync() throws InterruptedException;


    @Override
    MyProgressiveFuture<V> syncUninterruptibly();

    @Override
    MyProgressiveFuture<V> await() throws InterruptedException;

    @Override
    MyProgressiveFuture<V> awaitUninterruptibly();
}
