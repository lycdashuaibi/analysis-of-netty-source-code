# JUC包中的future

```java
class Test {
    public static void main(String[] args) {
        FutureTask<Boolean> hTask = new FutureTask<>(() -> {
            System.out.println(getCurrentThreadName() + "---------");
            System.out.println("洗好茶壶");
            System.out.println("灌上凉水");
            System.out.println(getCurrentThreadName() + "---------");
            //线程睡眠一段时间代表烧水中
            try {
                Thread.sleep(SLEEP_GAP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return false;
        });
    }
}
```

- 可以通过get方法获取异步任务操作的结果
- 若异步任务未完成，则会阻塞在get方法
- 如果到达超时时间操作仍然没有完成,则抛出 TimeoutException。
![](图片/Netty中的future图片/img_5.png)

# Netty中的future

![img_6.png](图片/Netty中的future图片/img_6.png)

- 继承于juc的future 多了增加监听器的功能
- netty推荐添加监听器来获取io结果

```java
public interface MyGenericFutureListener<F extends MyFuture<?>> extends EventListener {


    /**
     * 异步任务完成后的回调函数
     *
     * @param future 传入一个netty子类的future
     * @throws Exception 异步任务的错误
     */
    void operationComplete(F future) throws Exception;
}
```

在GenericFutureListener中，有一个回调函数 io线程操作完成后会调用这个函数 ChannelFuture 有两种状态： complated 和uncomplated

- netty的异步任务都是io事件，都在Channel里处理因此创建了ChannelFuture

## ChannelFuture

![img_7.png](图片/Netty中的future图片/img_7.png)

- 连接了channel，可以找回future中关联channel 推荐通过 GenericFutureListener代替 ChannelFuture的get等方法的
  当我们进行异步IO操作时,完成的时间是无法预测的,如果不设置超时时间,它会导致调用线程长时间被阻塞,甚至挂死。而设置超时时间,时间又无法精确预测。 利用异步通知机制回调
  GenericFutureListener是最佳的解决方案,它的性能最优。

因为future中没有设置io结果的方法，netty使用Promise来设置io结果

![img_8.png](图片/Netty中的future图片/img_8.png)

```java
class channel {
    void write(Object msg, ChannelPromise promise);
}
```

在io事件发生错误后，调用promise的tryFailure设置错误结果

# DefaultPromise

Promise的默认实现子类 自己实现一个默认的Promise

![img_9.png](图片/Netty中的future图片/img_9.png)

```java
public abstract class MyAbstractFuture<V> implements MyFuture<V> {
    @Override
    public V get() throws InterruptedException, ExecutionException {
        //先阻塞
        this.await();
        Throwable cause = cause();
        //没有抛出的错误，就获取结果
        if (cause == null) {
            return getNow();
        }
        if (cause instanceof CancellationException) {
            throw (CancellationException) cause;
        }
        throw new ExecutionException(cause);
    }

    @Override
    public V get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        if (await(timeout, unit)) {
            Throwable cause = cause();
            if (cause == null) {
                return getNow();
            }
            if (cause instanceof CancellationException) {
                throw (CancellationException) cause;
            }
            throw new ExecutionException(cause);
        }
        throw new TimeoutException();
    }
}
```

重要的是如何实现SetSuccess方法，给自身设置一个成功的状态

```java
public class MyDefaultPromise<V> extends MyAbstractFuture<V> implements MyPromise<V> {
    @Override
    public MyPromise<V> setSuccess(V result) {
        /*
        如果能设置成功状态，则返回本身
        否则就抛出异常
         */
        if (setSuccess0(result)) {
            return this;
        }
        throw new IllegalArgumentException("complete already: " + this);
    }

    public boolean setSuccess0(V result) {
        return setValue0(result == null ? SUCCESS : result);
    }

    private boolean setValue0(Object objResult) {

        //使用cas原理更新result，避免并发修改异常
        if (RESULT_UPDATER.compareAndSet(this, null, objResult) ||
                RESULT_UPDATER.compareAndSet(this, UNCANCELLABLE, objResult)) {
            //如果有正在等待异步IO操作完成的用户线程或者其他系统线程,则唤醒所有正在等待的线程。
            if (checkNotifyWaiters()) {
                notifyListeners();
            }
            return true;
        }
        return false;

    }

    @Override
    public MyPromise<V> await() throws InterruptedException {
        //如果promise被设置，就直接返回
        if (isDone()) {
            return this;
        }


        if (Thread.interrupted()) {
            throw new InterruptedException(toString());
        }

        //保护性检验，查询是否死锁
        checkDeadLock();

        synchronized (this) {
            while (!isDone()) {
                incWaiters();
                try {
                    wait();
                } finally {
                    decWaiters();
                }
            }
        }
        return this;
    }
}
```

