# Reactor 模型

![img_1.png](img_1.png)

## Reactor

**适用于同步io**

1. 事件分离者等待某个事件或者另外一个操作的状态发生
2. 分离者就会将事件传给回调函数处理

## 经典Reactor

![img.png](图片/reactor模型的图片/img.png)

1. reactor 将Io事件转发给对应的handler
2. acceptor 处理连接事件

## Netty的Reactor 模型

![img_2.png](图片/reactor模型的图片/img_2.png)
**使用多Reactor模型**

1. 一个Reactor监控所有的连接请求
2. 多个子Reactor监控处理读写请求
3. 一个子Reactor属于独立的线程，每个成功连接的Channel的操作只由一个线程处理，避免了不必要的上下文切换。

### 多线程单Reactor模型

```java
/**
 * Reactor多线程模型
 */
public class Reactor {
    public static void main(String[] args) throws IOException {
        //获取一个Selector
        Selector selector = Selector.open();
        //开启一个服务器channel
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        //非阻塞
        serverSocketChannel.configureBlocking(false);
        //绑定一个端口
        serverSocketChannel.bind(new InetSocketAddress(9000));
        //监听注册事件，将这个channel注册到selector上
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        while (true) {
            if (selector.selectNow() < 0) {
                continue;
            }
            //如果selectorKey集合有东西，就遍历事件
            Set<SelectionKey> keys = selector.selectedKeys();
            Iterator<SelectionKey> iterator = keys.iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                iterator.remove();
                //如果是连接事件，就拿到通道建立连接
                if (key.isAcceptable()) {
                    ServerSocketChannel acceptServerSocketChannel = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = acceptServerSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    System.out.println("Accept request from " + socketChannel.getRemoteAddress());
                    //注册一个readyKey到selector里
                    SelectionKey readKey = socketChannel.register(selector, SelectionKey.OP_READ);
                    //新建个线程处理绑定这个key
                    readKey.attach(new Processor());
                    //处理读事件
                } else if (key.isReadable()) {
                    Processor processor = (Processor) key.attachment();
                    processor.process(key);
                }
            }
        }
    }
}

class Processor {
    private static final ExecutorService SERVICE = Executors.newFixedThreadPool(16);

    public void process(SelectionKey selectionKey) {
        //线程池提交任务
        SERVICE.submit(() -> {
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            SocketChannel socketChannel = (SocketChannel) selectionKey.channel();
            int count = socketChannel.read(buffer);
            if (count < 0) {
                socketChannel.close();
                selectionKey.cancel();
                System.out.println(socketChannel + "\t Read ended");
                return null;
            } else if (count == 0) {
                return null;
            }
            System.out.println(socketChannel + "\t Read message " + new String(buffer.array()));
            return null;
        });
    }
}
```

一个NIO线程可以处理多个链路，但一个链路只会对应一个线程 一般可以使用，但大规模的连接事件或者nio的线程不稳定具有代价

### 主从Reactor多线程模型

接收连接是一个线程池，然后将这个连接注册到IO线程池去处理。

1. 从主线程池中随机选择一个Reactor线程作为Acceptor
2. 创建新的SocketChannel，注册到主线程池的reactor上
3. 将socketchannel摘除，注册到子线程池的reactor 上

```java
public class ReacorMutex {
    public static void main(String[] args) throws IOException {
        Selector selector = Selector.open();
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.bind(new InetSocketAddress(9000));
        serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
        int coreNum = Runtime.getRuntime().availableProcessors();
        Processor1[] processors = new Processor1[coreNum];
        for (int i = 0; i < processors.length; i++) {
            processors[i] = new Processor1();
        }
        System.out.println("initialized ...");
        int index = 0;
        while (selector.select() > 0) {
            Set<SelectionKey> keys = selector.selectedKeys();
            for (SelectionKey key : keys) {
                keys.remove(key);
                if (key.isAcceptable()) {
                    ServerSocketChannel acceptServerSocketChannel = (ServerSocketChannel) key.channel();
                    SocketChannel socketChannel = acceptServerSocketChannel.accept();
                    socketChannel.configureBlocking(false);
                    System.out.println("Accept request from " + socketChannel.getRemoteAddress());
                    Processor1 processor = processors[(int) ((index++) / coreNum)];
                    processor.addChannel(socketChannel);
                }
            }
        }
    }
}

class Processor1 {
    private static final ExecutorService executor = Executors.newFixedThreadPool(2 * Runtime.getRuntime().availableProcessors());
    private Selector selector;

    public Processor1() throws IOException {
        this.selector = Selector.open();
        start();
    }

    public void addChannel(SocketChannel socketChannel) throws ClosedChannelException {
        socketChannel.register(this.selector, SelectionKey.OP_READ);
    }

    public void start() {
        executor.submit(() -> {
            while (true) {
                if (selector.selectNow() <= 0) {
                    continue;
                }
                Set<SelectionKey> keys = selector.selectedKeys();
                Iterator<SelectionKey> iterator = keys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    iterator.remove();
                    if (key.isReadable()) { // 这部分可以交给用户设置的业务线程池处理，在 Netty 里应该是 对应着 ChannelHandler 的 channelRead 方法
                        ByteBuffer buffer = ByteBuffer.allocate(1024);
                        SocketChannel socketChannel = (SocketChannel) key.channel();
                        int count = socketChannel.read(buffer);
                        if (count < 0) {
                            socketChannel.close();
                            key.cancel();
                            System.out.println(socketChannel + "\t Read ended");
                            continue;
                        } else if (count == 0) {
                            System.out.println(socketChannel + "\t Message size is 0");
                            continue;
                        } else {
                            System.out.println(socketChannel + "\t Read message " + new String(buffer.array()));
                        }
                    }
                }
            }
        });
    }
}

```

# Netty Reactor实现

![img_3.png](图片/reactor模型的图片/img_3.png)
服务器启动后创建两个**NioEventLoopGroup**，两个独立的Reactor线程池

1. BossGroup线程池

- 接收客户端连接，初始化channel
- 将链路状态变更时间通知给ChannelPipeLine

```java
public class NettyReactor初始化 {

    public static void main(String[] args) {
        int port = 9999;
        //创建两个Nio线程
        NioEventLoopGroup bossGroup = new NioEventLoopGroup();
        NioEventLoopGroup workerGroup = new NioEventLoopGroup();
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class);
        try {
            ChannelFuture channelFuture = serverBootstrap.bind(port).sync();
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }
}
```

## netty如何创建Nio线程池？

Netty Nio线程池的继承图
![img_4.png](图片/reactor模型的图片/img_4.png)

1. 支持异步任务，继承了Executor
2. 支持定时任务

## 创建NioEventLoopGroup流程

```java
class Test {
    //1. 创建线程组的办法
    NioEventLoopGroup bossGroup = new NioEventLoopGroup();
    NioEventLoopGroup workerGroup = new NioEventLoopGroup(16);

    //2. 无参构造默认传0
    public NioEventLoopGroup() {
        this(0);
    }

    public NioEventLoopGroup(int nThreads) {
        this(nThreads, (Executor) null);
    }

    public NioEventLoopGroup(int nThreads, Executor executor) {
        this(nThreads, executor, SelectorProvider.provider());
    }

    public NioEventLoopGroup(
            int nThreads, Executor executor, final SelectorProvider selectorProvider) {
        this(nThreads, executor, selectorProvider, DefaultSelectStrategyFactory.INSTANCE);
    }

    public NioEventLoopGroup(int nThreads, Executor executor, final SelectorProvider selectorProvider,
                             final SelectStrategyFactory selectStrategyFactory) {
        super(nThreads, executor, selectorProvider, selectStrategyFactory, RejectedExecutionHandlers.reject());
    }

    //3. 访问上一层的多线程eventloop类
    protected MultithreadEventLoopGroup(int nThreads, Executor executor, Object... args) {
        super(nThreads == 0 ? DEFAULT_EVENT_LOOP_THREADS : nThreads, executor, args);
    }

    //如果是默认值，就初始化16个线程，否则就按传入的线程数创建
    protected MultithreadEventExecutorGroup(int nThreads, Executor executor, Object... args) {
        this(nThreads, executor, DefaultEventExecutorChooserFactory.INSTANCE, args);
    }

    protected MultithreadEventExecutorGroup(int nThreads, Executor executor,
                                            EventExecutorChooserFactory chooserFactory, Object... args) {

        //检验传入的线程
        if (nThreads <= 0) {
            throw new IllegalArgumentException(String.format("nThreads: %d (expected: > 0)", nThreads));
        }
        //如果没有初始化executor 就初始化
        //创建一个线程选择器
        if (executor == null) {
            executor = new ThreadPerTaskExecutor(newDefaultThreadFactory());
        }
        //数组
        children = new EventExecutor[nThreads];

        //不停的new 出处理线程
        for (int i = 0; i < nThreads; i++) {
            boolean success = false;
            try {
                children[i] = newChild(executor, args);
                success = true;
            } catch (Exception e) {
                throw new IllegalStateException("failed to create a child event loop", e);
            } finally {
                if (!success) {
                    //如果失败了，就删掉之前的线程
                    for (int j = 0; j < i; j++) {
                        children[j].shutdownGracefully();
                    }

                    for (int j = 0; j < i; j++) {
                        EventExecutor e = children[j];
                        try {
                            while (!e.isTerminated()) {
                                e.awaitTermination(Integer.MAX_VALUE, TimeUnit.SECONDS);
                            }
                        } catch (InterruptedException interrupted) {
                            // Let the caller handle the interruption.
                            Thread.currentThread().interrupt();
                            break;
                        }
                    }
                }
            }
        }

        //chooser是干嘛的？
        //给每个用户分配线程
        chooser = chooserFactory.newChooser(children);

        //每个处理线程添加listener
        final FutureListener<Object> terminationListener = new FutureListener<Object>() {
            @Override
            public void operationComplete(Future<Object> future) throws Exception {
                if (terminatedChildren.incrementAndGet() == children.length) {
                    terminationFuture.setSuccess(null);
                }
            }
        };

        for (EventExecutor e : children) {
            e.terminationFuture().addListener(terminationListener);
        }
        Set<EventExecutor> childrenSet = new LinkedHashSet<EventExecutor>(children.length);
        Collections.addAll(childrenSet, children);
        readonlyChildren = Collections.unmodifiableSet(childrenSet);
    }
}
```
1. 实质上要创建一个Child数组
2. 看是否传入线程，不然就初始一个16个线程

