# Channel

Netty的抽象了一个顶层接口Channel相比原来NIO提供的Channel有更多的功能，当然也是相对复杂的。

# Java NIO包下的channel

![图](图片/Netty中的channel图片/img_1.png)
![](图片/Netty中的channel图片/img_3.png)
![img.png](图片/Netty中的channel图片/img_2.png)

JavaNIO包下的channel 做了三层封装
![img.png](图片/Netty中的channel图片/img_4.png)

1. 最顶层的Channel只封装了关和开两种状态
2. 第二层SelectableChannel封装了和Selector交互的状态
3. 第三层封装了具体的业务类

# Netty的channel

- 继承图
  ![图片1.png](图片/Netty中的channel图片/img.png)

做了四层封装

1. 第一层channel
   ![图片1.png](图片/Netty中的channel图片/img_5.png)
   抽象了所有的channel的公共行为

2. 第二层channel
   ![图片1.png](图片/Netty中的channel图片/img_6.png)
   规定了channel为Nio的抽象

3. 第三层channel
   ![图片1.png](图片/Netty中的channel图片/img_7.png)
   读写层的抽象

4. 第四层channel 具体业务的实现，比如serverSocketChannel

## AttributeMap

一个线程安全的map，附在channel或者ChannelHandlerContext上 Channel上的AttributeMap就是大家共享的，每一个ChannelHandler都能获取到
![图片1.png](图片/Netty中的channel图片/img_8.png)
[MyDefaultAttribute.java](/src/main/java/channel/attribute/MyDefaultAttributeMap.java)
AttributeMap可以看成是一个key为AttributeKey类型，value为Attribute类型的Map，而Attribute内存储的是一个值引用，它可以原子的更新内容，是线程安全的 pool常量池存放了key

```java
public final class MyAttributeKey<T> extends AbstractConstant<MyAttributeKey<T>> {

    private static final ConstantPool<MyAttributeKey<Object>> pool = new ConstantPool<MyAttributeKey<Object>>() {
        @Override
        protected MyAttributeKey<Object> newConstant(int id, String name) {
            return new MyAttributeKey<>(id, name);
        }
    };
}
```

AttributeMap接口的默认实现类是DefaultAttributeMap类，该类有一个内部类DefaultAttribute，实现了Attribute接口，其定义如下：

```java
private static final class MyDefaultAttribute<T> extends AtomicReference<T> implements MyAttribute<T> {

    private static final long serialVersionUID = 4618134439190501308L;
    private static final AtomicReferenceFieldUpdater<MyDefaultAttribute, MyDefaultAttributeMap> MAP_UPDATER =
            AtomicReferenceFieldUpdater.newUpdater(MyDefaultAttribute.class,
                    MyDefaultAttributeMap.class, "attributeMap");
    private final MyAttributeKey<T> key;
    private volatile MyDefaultAttributeMap attributeMap;

    MyDefaultAttribute(MyDefaultAttributeMap attributeMap, MyAttributeKey<T> key) {
        this.attributeMap = attributeMap;
        this.key = key;
    }

    @Override
    public MyAttributeKey<T> key() {
        return key;
    }

    @Override
    public T setIfAbsent(T value) {
        while (!compareAndSet(null, value)) {
            T old = get();
            if (old != null) {
                return old;
            }
        }
        return null;
    }

    @Override
    public T getAndRemove() {
        MyDefaultAttributeMap attributeMap = this.attributeMap;
        boolean removed = attributeMap != null && MAP_UPDATER.compareAndSet(this, attributeMap, null);
        T oldValue = getAndSet(null);
        if (removed) {
            attributeMap.removeAttributeIfMatch(key, this);
        }

        return oldValue;
    }

    @Override
    public void remove() {
        final MyDefaultAttributeMap attributeMap = this.attributeMap;
        final boolean removed = attributeMap != null && MAP_UPDATER.compareAndSet(this, attributeMap, null);
        set(null);
        if (removed) {
            attributeMap.removeAttributeIfMatch(key, this);
        }
    }

    private boolean isRemoved() {
        return attributeMap == null;
    }
}
```

该类继承了AtomicReference类，这是Netty内部实现的一个原子引用类，因此DefaultAttribute是线程安全的。它是一个双向链表的节点，也就是所有的Attribute连接成了一个双向链表的存储结构。而DefaultAttribute对象被存放在了一个AtomicReferenceArray类型的数组结构的属性attributes里面，这是AttributeMap内部的核心存储结构。

# Netty启动的时候怎么创建channel

## 创建channelfactory

ServerBootStrap.channel 返回一个serverbootstrap, 提交一个channel类

```java
class Test {
    public B channel(Class<? extends C> channelClass) {
        return channelFactory(new ReflectiveChannelFactory<C>(
                ObjectUtil.checkNotNull(channelClass, "channelClass")
        ));
    }
}
```

反射加载: 获取到类的加载器,构建一个channelfactory返回到abstractBootStrap中

```java
class Test {

    public ReflectiveChannelFactory(Class<? extends T> clazz) {
        ObjectUtil.checkNotNull(clazz, "clazz");
        try {
            this.constructor = clazz.getConstructor();
        } catch (NoSuchMethodException e) {
            throw new IllegalArgumentException("Class " + StringUtil.simpleClassName(clazz) +
                    " does not have a public non-arg constructor", e);
        }
    }

    //AbstractBootStrap 中
    public B channelFactory(ChannelFactory<? extends C> channelFactory) {
        ObjectUtil.checkNotNull(channelFactory, "channelFactory");
        if (this.channelFactory != null) {
            throw new IllegalStateException("channelFactory set already");
        }

        this.channelFactory = channelFactory;
        return self();
    }
}
```

加载完之后,用户调用bind方法绑定端口,这时候才创建channel

```java
class Bind {
    public ChannelFuture bind(int inetPort) {
        return bind(new InetSocketAddress(inetPort));
    }

    public ChannelFuture bind(SocketAddress localAddress) {
        validate();//这里会做检验,如果上文提到的channelfactory没有创建,就会抛出异常
        return doBind(ObjectUtil.checkNotNull(localAddress, "localAddress"));
    }

    private ChannelFuture doBind(final SocketAddress localAddress) {
        final ChannelFuture regFuture = initAndRegister();
        //在dobind第一行就调用了initAndRegister
    }

    final ChannelFuture initAndRegister() {
        Channel channel = null;
        try {
            //调用反射方法获得channel
            channel = channelFactory.newChannel();
            //初始化channel
            init(channel);
        } catch (Throwable t) {
            if (channel != null) {
                // channel can be null if newChannel crashed (eg SocketException("too many open files"))
                channel.unsafe().closeForcibly();
                // as the Channel is not registered yet we need to force the usage of the GlobalEventExecutor
                return new DefaultChannelPromise(channel, GlobalEventExecutor.INSTANCE).setFailure(t);
            }
            // as the Channel is not registered yet we need to force the usage of the GlobalEventExecutor
            return new DefaultChannelPromise(new FailedChannel(), GlobalEventExecutor.INSTANCE).setFailure(t);
        }

        ChannelFuture regFuture = config().group().register(channel);
        if (regFuture.cause() != null) {
            if (channel.isRegistered()) {
                channel.close();
            } else {
                channel.unsafe().closeForcibly();
            }
        }
        return regFuture;
    }
}
```

# 如何反射创建服务端channel?

1. newSocket()通过jdk来创建底层的jdk channel
2. 配置tcp参数配置类 NioServerSocketChannelConfig
3. 设置Nio非阻塞模式
4. 创建id,unsafe,pipeline等channel独有的参数 NioSocketChannel的构造函数:

## 创建底层的jdkchannel

```java
class NioServerSocketChannel {
    private static final SelectorProvider DEFAULT_SELECTOR_PROVIDER = SelectorProvider.provider();

    public NioServerSocketChannel() {
        this(newSocket(DEFAULT_SELECTOR_PROVIDER));
    }

    private static ServerSocketChannel newSocket(SelectorProvider provider) {
        try {
            return provider.openServerSocketChannel();
        } catch (IOException e) {
            throw new ChannelException(
                    "Failed to open a server socket.", e);
        }
    }

    ServerSocketChannelImpl(SelectorProvider sp) throws IOException {
        super(sp);
        this.fd = Net.serverSocket(true);
        this.fdVal = IOUtil.fdVal(fd);
        this.state = ST_INUSE;
    }

    //创建底层的socket
    static FileDescriptor serverSocket(boolean stream) {
        return IOUtil.newFD(socket0(isIPv6Available(), stream, true, fastLoopback));
    }
}
```

## 配置tcp参数配置类

```java
class NioServerSocketChannelConfig {
    public NioServerSocketChannel(ServerSocketChannel channel) {
        super(null, channel, SelectionKey.OP_ACCEPT);
        config = new NioServerSocketChannelConfig(this, javaChannel().socket());
    }
}
```

## 设置非阻塞模式

```java
class AbstractNioChannel {
    protected AbstractNioChannel(Channel parent, SelectableChannel ch, int readInterestOp) {
        super(parent);
        this.ch = ch;
        this.readInterestOp = readInterestOp;
        try {
            //设置为非阻塞模式
            ch.configureBlocking(false);
        } catch (IOException e) {
            try {
                ch.close();
            } catch (IOException e2) {
                logger.warn(
                        "Failed to close a partially initialized socket.", e2);
            }

            throw new ChannelException("Failed to enter non-blocking mode.", e);
        }
    }
}
```

## 创建一些独特参数

```java
class AbstractChannel {
    protected AbstractChannel(Channel parent) {
        this.parent = parent;
        id = newId();
        unsafe = newUnsafe();
        pipeline = newChannelPipeline();
    }
}

```

# 初始化服务端channel

1. 配置用户自定义的Channel Option,ChannelAttrs
2. 配置ChildOptions,ChildAttrs
3. 配置服务端pipeline
4. 添加一个ServerBootstrapAcceptor 添加连接器

```java
class ServerBootstrap {
    @Override
    void init(Channel channel) {
        //设置自定义option
        setChannelOptions(channel, newOptionsArray(), logger);
        //设置自定义attributes
        setAttributes(channel, newAttributesArray());
        //拿到channel的pipeline
        ChannelPipeline p = channel.pipeline();

        final EventLoopGroup currentChildGroup = childGroup;
        final ChannelHandler currentChildHandler = childHandler;
        final Entry<ChannelOption<?>, Object>[] currentChildOptions = newOptionsArray(childOptions);
        final Entry<AttributeKey<?>, Object>[] currentChildAttrs = newAttributesArray(childAttrs);

        //添加处理器链服务端的handler
        p.addLast(new ChannelInitializer<Channel>() {
            @Override
            public void initChannel(final Channel ch) {
                final ChannelPipeline pipeline = ch.pipeline();
                ChannelHandler handler = config.handler();
                if (handler != null) {
                    pipeline.addLast(handler);
                }

                ch.eventLoop().execute(new Runnable() {
                    @Override
                    public void run() {
                        //默认的处理器
                        pipeline.addLast(new ServerBootstrapAcceptor(
                                ch, currentChildGroup, currentChildHandler, currentChildOptions, currentChildAttrs));
                    }
                });
            }
        });
    }
}
```

# channel注册到selector

1. 绑定线程
2. 实际注册
3. jdk底层注册
4. 注册回调
5. fireChannelRegistered() 传播事件

## AbstractChannel的unsafe包里

```java

class register {
    //在init and regist 方法里
    ChannelFuture regFuture = config().group().register(channel);

    @Override
    public ChannelFuture register(Channel channel) {
        return register(new DefaultChannelPromise(channel, this));
    }

    @Override
    public ChannelFuture register(ChannelPromise promise) {
        ObjectUtil.checkNotNull(promise, "promise");
        promise.channel().unsafe().register(this, promise);
        return promise;
    }

    @Override
    public final void register(EventLoop eventLoop, final ChannelPromise promise) {
        ObjectUtil.checkNotNull(eventLoop, "eventLoop");
        //判断是否已经注册到一个事件集合
        if (isRegistered()) {
            promise.setFailure(new IllegalStateException("registered to an event loop already"));
            return;
        }
        //事件集合是否准备完全
        if (!isCompatible(eventLoop)) {
            promise.setFailure(
                    new IllegalStateException("incompatible event loop type: " + eventLoop.getClass().getName()));
            return;
        }
        //当前eventloop
        AbstractChannel.this.eventLoop = eventLoop;

        if (eventLoop.inEventLoop()) {
            register0(promise);
        } else {
            try {
                eventLoop.execute(new Runnable() {
                    @Override
                    public void run() {
                        register0(promise);
                    }
                });
            } catch (Throwable t) {
                logger.warn(
                        "Force-closing a channel whose registration task was not accepted by an event loop: {}",
                        AbstractChannel.this, t);
                closeForcibly();
                closeFuture.setClosed();
                safeSetFailure(promise, t);
            }
        }
    }

    private void register0(ChannelPromise promise) {
        try {
            // check if the channel is still open as it could be closed in the mean time when the register
            // call was outside of the eventLoop
            if (!promise.setUncancellable() || !ensureOpen(promise)) {
                return;
            }
            boolean firstRegistration = neverRegistered;
            doRegister();
            neverRegistered = false;
            registered = true;

            // Ensure we call handlerAdded(...) before we actually notify the promise. This is needed as the
            // user may already fire events through the pipeline in the ChannelFutureListener.

            pipeline.invokeHandlerAddedIfNeeded();

            safeSetSuccess(promise);
            pipeline.fireChannelRegistered();
            // Only fire a channelActive if the channel has never been registered. This prevents firing
            // multiple channel actives if the channel is deregistered and re-registered.
            if (isActive()) {
                if (firstRegistration) {
                    pipeline.fireChannelActive();
                } else if (config().isAutoRead()) {
                    // This channel was registered before and autoRead() is set. This means we need to begin read
                    // again so that we process inbound data.
                    //
                    // See https://github.com/netty/netty/issues/4805
                    beginRead();
                }
            }
        } catch (Throwable t) {
            // Close the channel directly to avoid FD leak.
            closeForcibly();
            closeFuture.setClosed();
            safeSetFailure(promise, t);
        }
    }
}
```

## 底层创建jdkchannel

```java
class AbstractNioChannel {
    @Override
    protected void doRegister() throws Exception {
        boolean selected = false;
        for (; ; ) {
            try {
                //this:attchment绑定到selector上
                selectionKey = javaChannel().register(eventLoop().unwrappedSelector(), 0, this);
                return;
            } catch (CancelledKeyException e) {
                if (!selected) {
                    // Force the Selector to select now as the "canceled" SelectionKey may still be
                    // cached and not removed because no Select.select(..) operation was called yet.
                    eventLoop().selectNow();
                    selected = true;
                } else {
                    // We forced a select operation on the selector before but the SelectionKey is still cached
                    // for whatever reason. JDK bug ?
                    throw e;
                }
            }
        }
    }
}
```

## 注册回调

调用用户handler里面的channeladd ed方法

```java
class AbstractChannel {
    public static void main(String[] args) {
        pipeline.invokeHandlerAddedIfNeeded();
    }

    final void invokeHandlerAddedIfNeeded() {
        assert channel.eventLoop().inEventLoop();
        if (firstRegistration) {
            firstRegistration = false;
            // We are now registered to the EventLoop. It's time to call the callbacks for the ChannelHandlers,
            // that were added before the registration was done.
            callHandlerAddedForAllHandlers();
        }
    }

    private void callHandlerAddedForAllHandlers() {
        final PendingHandlerCallback pendingHandlerCallbackHead;
        //加锁,保证单线程回调所有handleradd方法
        synchronized (this) {
            assert !registered;

            // This Channel itself was registered.
            registered = true;

            pendingHandlerCallbackHead = this.pendingHandlerCallbackHead;
            // Null out so it can be GC'ed.
            this.pendingHandlerCallbackHead = null;
        }

        PendingHandlerCallback task = pendingHandlerCallbackHead;
        while (task != null) {
            task.execute();
            task = task.next;
        }
    }

}

```

然后调用pipeline.fireChannelRegistered();来调用全部的handlerRegistered方法

**注意! 此时isActive()方法返回false,因为还没有绑定端口**

# 端口绑定

1. unsafe中的bind方法
2. jdk底层绑定
3. firechannelActive,使得所有的channelActive事件生效
4. 重新更新headcontextselector事件为注册事件

```java
class Bind {
    private static void doBind0(
            final ChannelFuture regFuture, final Channel channel,
            final SocketAddress localAddress, final ChannelPromise promise) {

        // This method is invoked before channelRegistered() is triggered.  Give user handlers a chance to set up
        // the pipeline in its channelRegistered() implementation.
        channel.eventLoop().execute(new Runnable() {
            @Override
            public void run() {
                //前面channelregister成功后
                if (regFuture.isSuccess()) {
                    channel.bind(localAddress, promise).addListener(ChannelFutureListener.CLOSE_ON_FAILURE);
                } else {
                    promise.setFailure(regFuture.cause());
                }
            }
        });
    }

    //在abstractChannel.unsafe中
    @Override
    public final void bind(final SocketAddress localAddress, final ChannelPromise promise) {
        //判断是否注册
        assertEventLoop();

        if (!promise.setUncancellable() || !ensureOpen(promise)) {
            return;
        }

        // See: https://github.com/netty/netty/issues/576
        if (Boolean.TRUE.equals(config().getOption(ChannelOption.SO_BROADCAST)) &&
                localAddress instanceof InetSocketAddress &&
                !((InetSocketAddress) localAddress).getAddress().isAnyLocalAddress() &&
                !PlatformDependent.isWindows() && !PlatformDependent.maybeSuperUser()) {
            // Warn a user about the fact that a non-root user can't receive a
            // broadcast packet on *nix if the socket is bound on non-wildcard address.
            logger.warn(
                    "A non-root user can't receive a broadcast packet if the socket " +
                            "is not bound to a wildcard address; binding to a non-wildcard " +
                            "address (" + localAddress + ") anyway as requested.");
        }

        boolean wasActive = isActive();
        try {
            doBind(localAddress);
        } catch (Throwable t) {
            safeSetFailure(promise, t);
            closeIfClosed();
            return;
        }

        if (!wasActive && isActive()) {
            invokeLater(new Runnable() {
                @Override
                public void run() {
                    //调用channel的active事件
                    pipeline.fireChannelActive();
                }
            });
        }

        safeSetSuccess(promise);
    }

    @SuppressJava6Requirement(reason = "Usage guarded by java version check")
    @Override
    protected void doBind(SocketAddress localAddress) throws Exception {
        if (PlatformDependent.javaVersion() >= 7) {
            javaChannel().bind(localAddress, config.getBacklog());
        } else {
            javaChannel().socket().bind(localAddress, config.getBacklog());
        }
    }
}
```