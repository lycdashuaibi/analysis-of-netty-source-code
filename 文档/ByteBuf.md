# ByteBuf结构

![1.png](图片/bytebuf的图片/img.png)

## 三个重要的指针

1. readIndex
2. writeIndex
3. capacity

分隔了三个区域: 不可使用区域,可读区域,可写区域

## 标记读指针,写指针

可以读完之后复原

```java
class ByteBuf {
    public abstract ByteBuf markReaderIndex();

    public abstract ByteBuf resetReaderIndex();

}
```

# ButeBuf的分类

![1.png](图片/bytebuf的图片/img_1.png)
顶层继承了一个引用计数器,方便释放内存

## AbstractByteBuf

实现了bytebuf的基本骨架

1. 保存了读写指针变量
2. 子类实现写和读的具体操作,父类定义逻辑,模板设计方式

```java
class AbstractByteBuf {
    int readerIndex;
    int writerIndex;
    private int markedReaderIndex;
    private int markedWriterIndex;
    private int maxCapacity;
}
```

## Pooled和Unpooled

池化或者不池化

- 是从内存池拿,还是直接从操作系统申请

## unsafe和safe

是否可以直接拿到对象的内存地址(在jvm中)

```java
class UnpooledUnsafeHeapByteBuf {
    @Override
    protected byte _getByte(int index) {
        //使用unsafe工具类获得
        return UnsafeByteBufUtil.getByte(array, index);
    }

    static byte getByte(long address) {
        return PlatformDependent.getByte(address);
    }

    public static byte getByte(long address) {
        return PlatformDependent0.getByte(address);
    }

    static byte getByte(long address) {
        return UNSAFE.getByte(address);
    }

    //跟踪到native方法
    public native byte getByte(long address);
}
```

## Heap和direct

堆内存和直接内存(不受jvm管理),不会gc 堆不需要自己释放 direct需要自己释放

# BytebufAllocator

内存分配器
![1.png](图片/bytebuf的图片/img_2.png)

## 功能

1. 计算新的容量
2. 分配堆内存
3. 直接内存分配
4. 复合内存分配(较少使用)
5. io缓冲区的操作(更偏向分配直接内存)

# 有关名词解释

## arena

开辟一块内存
![1.png](图片/bytebuf的图片/img_4.png)
维护了一个双向链表,双向chunklist netty会计算chunk的使用情况,按照内存使用率划分chunklist 然后通过二分查找找到对应

- chunlist q050 = new PoolChunkList<T>(this, q075, 50, 100, chunkSize);

## page

chunk划分为8k的page(2000个page)

## subpage

page还可以划分为subpage

## AbstractByteBufAllocator

```java
class AbstractByteBufAllocator {
    @Override
    public ByteBuf buffer() {
        if (directByDefault) {
            return directBuffer();
        }
        return heapBuffer();
    }

    @Override
    public ByteBuf directBuffer() {
        return directBuffer(DEFAULT_INITIAL_CAPACITY, DEFAULT_MAX_CAPACITY);
    }

    @Override
    public ByteBuf directBuffer(int initialCapacity, int maxCapacity) {
        if (initialCapacity == 0 && maxCapacity == 0) {
            return emptyBuf;
        }
        validate(initialCapacity, maxCapacity);
        return newDirectBuffer(initialCapacity, maxCapacity);
    }

    //最终委托给子类实现
    protected abstract ByteBuf newDirectBuffer(int initialCapacity, int maxCapacity);
}
```

## UnpooledByteBufAllocator

final 类

### heap内存分配

```java
class UnpooledByteBufAllocator {
    @Override
    protected ByteBuf newHeapBuffer(int initialCapacity, int maxCapacity) {
        return PlatformDependent.hasUnsafe() ?
                new InstrumentedUnpooledUnsafeHeapByteBuf(this, initialCapacity, maxCapacity) :
                new InstrumentedUnpooledHeapByteBuf(this, initialCapacity, maxCapacity);
    }

    InstrumentedUnpooledHeapByteBuf(UnpooledByteBufAllocator alloc, int initialCapacity, int maxCapacity) {
        super(alloc, initialCapacity, maxCapacity);
    }

    public UnpooledHeapByteBuf(ByteBufAllocator alloc, int initialCapacity, int maxCapacity) {

        //最终调用abstractBytebuf设置最大的容量
        super(maxCapacity);

        if (initialCapacity > maxCapacity) {
            throw new IllegalArgumentException(String.format(
                    "initialCapacity(%d) > maxCapacity(%d)", initialCapacity, maxCapacity));
        }

        this.alloc = checkNotNull(alloc, "alloc");
        //创建一个新的字节数组
        setArray(allocateArray(initialCapacity));
        //设置坐标
        setIndex(0, 0);
    }
}
```

## pooledByteBufAllocator

1. 拿到线程局部缓存PoolThreadCache（不同线程不同区域分配）本质是个threadLocal
2. 在线程局部缓存的area上进行内存分配

```java
class pooledByteBufAllocator {
    @Override
    protected ByteBuf newHeapBuffer(int initialCapacity, int maxCapacity) {
        PoolThreadCache cache = threadCache.get();
        PoolArena<byte[]> heapArena = cache.heapArena;

        final ByteBuf buf;
        if (heapArena != null) {
            buf = heapArena.allocate(cache, initialCapacity, maxCapacity);
        } else {
            buf = PlatformDependent.hasUnsafe() ?
                    new UnpooledUnsafeHeapByteBuf(this, initialCapacity, maxCapacity) :
                    new UnpooledHeapByteBuf(this, initialCapacity, maxCapacity);
        }

        return toLeakAwareBuffer(buf);
    }

    final class PoolThreadLocalCache extends FastThreadLocal<PoolThreadCache> {
        @Override
        protected synchronized PoolThreadCache initialValue() {
            //分配堆内存和直接内存 并保存为成员变量
            final PoolArena<byte[]> heapArena = leastUsedArena(heapArenas);
            final PoolArena<ByteBuffer> directArena = leastUsedArena(directArenas);

            final Thread current = Thread.currentThread();
            if (useCacheForAllThreads || current instanceof FastThreadLocalThread) {
                final PoolThreadCache cache = new PoolThreadCache(
                        heapArena, directArena, smallCacheSize, normalCacheSize,
                        DEFAULT_MAX_CACHED_BUFFER_CAPACITY, DEFAULT_CACHE_TRIM_INTERVAL);

                final EventExecutor executor = ThreadExecutorM
                if (DEFAULT_CACHE_TRIM_INTERVAL_MILLIS > 0) {
                    ap.currentExecutor();
                    if (executor != null) {
                        executor.scheduleAtFixedRate(trimTask, DEFAULT_CACHE_TRIM_INTERVAL_MILLIS,
                                DEFAULT_CACHE_TRIM_INTERVAL_MILLIS, TimeUnit.MILLISECONDS);
                    }
                }
                return cache;
            }
            // No caching so just use 0 as sizes.
            return new PoolThreadCache(heapArena, directArena, 0, 0, 0, 0);
        }

        private <T> PoolArena<T> leastUsedArena(PoolArena<T>[] arenas) {
            if (arenas == null || arenas.length == 0) {
                return null;
            }

            PoolArena<T> minArena = arenas[0];
            for (int i = 1; i < arenas.length; i++) {
                PoolArena<T> arena = arenas[i];
                if (arena.numThreadCaches.get() < minArena.numThreadCaches.get()) {
                    minArena = arena;
                }
            }

            return minArena;
        }

        @SuppressWarnings("unchecked")
        private static <T> PoolArena<T>[] newArenaArray(int size) {
            return new PoolArena[size];
        }
    }

    final int defaultMinNumArena = NettyRuntime.availableProcessors() * 2;
    int DEFAULT_NUM_HEAP_ARENA = Math.max(0, SystemPropertyUtil.getInt("io.netty.allocator.numHeapArenas",
            (int) Math.min(
                    defaultMinNumArena,
                    runtime.maxMemory() / defaultChunkSize / 2 / 3)));
}
```

默认堆内存为cpu核数的两倍（每一个线程都有自己的arena）不需要加锁

### 结构

![1.png](图片/bytebuf的图片/img_3.png)

# DirectArena 分配direct内存的过程

1. 从对象池拿到PooledByteBuf进行复用
2. 从缓存上进行内存分配
3. 如果缓存不够,再从内存堆里进行内存分配

```java
class PooledByteBufAllocator {
    private void allocate(PoolThreadCache cache, PooledByteBuf<T> buf, final int reqCapacity) {
        final int sizeIdx = size2SizeIdx(reqCapacity);

        if (sizeIdx <= smallMaxSizeIdx) {
            //缓存分配小空间(复用最小缓存)
            tcacheAllocateSmall(cache, buf, reqCapacity, sizeIdx);
        } else if (sizeIdx < nSizes) {
            //缓存重新分配
            tcacheAllocateNormal(cache, buf, reqCapacity, sizeIdx);
        } else {
            //从内存堆里进行内存分配
            int normCapacity = directMemoryCacheAlignment > 0
                    ? normalizeSize(reqCapacity) : reqCapacity;
            // Huge allocations are never served via the cache so just call allocateHuge
            allocateHuge(buf, normCapacity);
        }
    }
}
```

## 内存规格

0-512B-8K-16M

1. tiny
2. small
3. normal
4. huge

16M-chunk 8k-page  
一个chunk有2048个page 512B-Subpage

# 缓存数据结构 MemoryRegionCache

1. 队列,存放chunk和handler的队列,handler指向内存的位置,固定大小
2. 一个sizeClass,表明是多大的内存,内存规格
3. size:表明大小

tiny[32](新版本已经弃用)
small[4]
normal[3]

```java
 private abstract static class MemoryRegionCache<T> {
    private final int size;
    private final Queue<Entry<T>> queue;
    private final SizeClass sizeClass;
    private int allocations;

    MemoryRegionCache(int size, SizeClass sizeClass) {
        this.size = MathUtil.safeFindNextPositivePowerOfTwo(size);
        queue = PlatformDependent.newFixedMpscQueue(this.size);
        this.sizeClass = sizeClass;
    }

    protected abstract void initBuf(PoolChunk<T> chunk, ByteBuffer nioBuffer, long handle,
                                    PooledByteBuf<T> buf, int reqCapacity, PoolThreadCache threadCache);

    /**
     * Add to cache if not already full.
     */
    @SuppressWarnings("unchecked")
    public final boolean add(PoolChunk<T> chunk, ByteBuffer nioBuffer, long handle, int normCapacity) {
        Entry<T> entry = newEntry(chunk, nioBuffer, handle, normCapacity);
        boolean queued = queue.offer(entry);
        if (!queued) {
            // If it was not possible to cache the chunk, immediately recycle the entry
            entry.recycle();
        }

        return queued;
    }

    /**
     * Allocate something out of the cache if possible and remove the entry from the cache.
     */
    public final boolean allocate(PooledByteBuf<T> buf, int reqCapacity, PoolThreadCache threadCache) {
        Entry<T> entry = queue.poll();
        if (entry == null) {
            return false;
        }
        initBuf(entry.chunk, entry.nioBuffer, entry.handle, buf, reqCapacity, threadCache);
        entry.recycle();

        // allocations is not thread-safe which is fine as this is only called from the same thread all time.
        ++allocations;
        return true;
    }

    /**
     * Clear out this cache and free up all previous cached {@link PoolChunk}s and {@code handle}s.
     */
    public final int free(boolean finalizer) {
        return free(Integer.MAX_VALUE, finalizer);
    }

    private int free(int max, boolean finalizer) {
        int numFreed = 0;
        for (; numFreed < max; numFreed++) {
            Entry<T> entry = queue.poll();
            if (entry != null) {
                freeEntry(entry, finalizer);
            } else {
                // all cleared
                return numFreed;
            }
        }
        return numFreed;
    }

    /**
     * Free up cached {@link PoolChunk}s if not allocated frequently enough.
     */
    public final void trim() {
        int free = size - allocations;
        allocations = 0;

        // We not even allocated all the number that are
        if (free > 0) {
            free(free, false);
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private void freeEntry(Entry entry, boolean finalizer) {
        PoolChunk chunk = entry.chunk;
        long handle = entry.handle;
        ByteBuffer nioBuffer = entry.nioBuffer;

        if (!finalizer) {
            // recycle now so PoolChunk can be GC'ed. This will only be done if this is not freed because of
            // a finalizer.
            entry.recycle();
        }

        chunk.arena.freeChunk(chunk, handle, entry.normCapacity, sizeClass, nioBuffer, finalizer);
    }

    static final class Entry<T> {
        final Handle<Entry<?>> recyclerHandle;
        PoolChunk<T> chunk;
        ByteBuffer nioBuffer;
        long handle = -1;
        int normCapacity;

        Entry(Handle<Entry<?>> recyclerHandle) {
            this.recyclerHandle = recyclerHandle;
        }

        void recycle() {
            chunk = null;
            nioBuffer = null;
            handle = -1;
            recyclerHandle.recycle(this);
        }
    }

    @SuppressWarnings("rawtypes")
    private static Entry newEntry(PoolChunk<?> chunk, ByteBuffer nioBuffer, long handle, int normCapacity) {
        Entry entry = RECYCLER.get();
        entry.chunk = chunk;
        entry.nioBuffer = nioBuffer;
        entry.handle = handle;
        entry.normCapacity = normCapacity;
        return entry;
    }

    @SuppressWarnings("rawtypes")
    private static final ObjectPool<Entry> RECYCLER = ObjectPool.newPool(new ObjectCreator<Entry>() {
        @SuppressWarnings("unchecked")
        @Override
        public Entry newObject(Handle<Entry> handle) {
            return new Entry(handle);
        }
    });

    //新版本不区分tiny
    private final MemoryRegionCache<byte[]>[] smallSubPageHeapCaches;
    private final MemoryRegionCache<ByteBuffer>[] smallSubPageDirectCaches;
    private final MemoryRegionCache<byte[]>[] normalHeapCaches;
    private final MemoryRegionCache<ByteBuffer>[] normalDirectCaches;
}
```

## 缓存的初始化

```java
class PoolThreadCache {
    PoolThreadCache(PoolArena<byte[]> heapArena, PoolArena<ByteBuffer> directArena,
                    int smallCacheSize, int normalCacheSize, int maxCachedBufferCapacity,
                    int freeSweepAllocationThreshold) {
        checkPositiveOrZero(maxCachedBufferCapacity, "maxCachedBufferCapacity");
        this.freeSweepAllocationThreshold = freeSweepAllocationThreshold;
        this.heapArena = heapArena;
        this.directArena = directArena;
        //如果有直接内存区域,就初始化缓存
        if (directArena != null) {
            //初始化
            smallSubPageDirectCaches = createSubPageCaches(
                    smallCacheSize, directArena.numSmallSubpagePools);

            normalDirectCaches = createNormalCaches(
                    normalCacheSize, maxCachedBufferCapacity, directArena);

            directArena.numThreadCaches.getAndIncrement();
        }
        if (heapArena != null) {
            // Create the caches for the heap allocations
            smallSubPageHeapCaches = createSubPageCaches(
                    smallCacheSize, heapArena.numSmallSubpagePools);

            normalHeapCaches = createNormalCaches(
                    normalCacheSize, maxCachedBufferCapacity, heapArena);

            heapArena.numThreadCaches.getAndIncrement();
        }
    }

    private static <T> MemoryRegionCache<T>[] createSubPageCaches(
            int cacheSize, int numCaches) {
        if (cacheSize > 0 && numCaches > 0) {
            @SuppressWarnings("unchecked")
            //创建一个数组(normal和small规格的)
            //根据cacheSize生成small规格的内存
            MemoryRegionCache<T>[] cache = new MemoryRegionCache[numCaches];
            for (int i = 0; i < cache.length; i++) {
                // TODO: maybe use cacheSize / cache.length
                cache[i] = new SubPageMemoryRegionCache<T>(cacheSize);
            }
            return cache;
        } else {
            return null;
        }
    }
}
```
# ByteBuf 的释放
1. 连续的内存区段加到缓存
2. 标记连续的内存区段未使用
3. ByteBuf加入到对象池