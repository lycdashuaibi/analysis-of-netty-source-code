# FastThreadLocal

## 使用

```java
public class FastThreadLocalTest {

    //static  Object  object = new Object();
    //唯一的id
    private static FastThreadLocal<Object> threadLocal = new FastThreadLocal<Object>() {
        @Override
        protected Object initialValue() throws Exception {
            return new Object();
        }
    };

    public static void main(String[] args) {
        new Thread(() -> {
            Object obj = threadLocal.get();
            // do with object
            System.out.println(obj);
        }).start();
        new Thread(() -> {
            Object obj = threadLocal.get();
            // do with object
            System.out.println(obj);
        }).start();
    }

}
```

结果:拿到两个不同的对象 每个线程拿出来的对象都是本线程独享 其他线程修改变量不会影响当前线程

## FastThreadLocal 的get方法

```java
class FastThreadLocal {
    public static InternalThreadLocalMap get() {
        Thread thread = Thread.currentThread();
        if (thread instanceof FastThreadLocalThread) {
            return fastGet((FastThreadLocalThread) thread);
        } else {
            return slowGet();
        }
    }

    public final V get() {
        //index索引在map具体的对象
        InternalThreadLocalMap threadLocalMap = InternalThreadLocalMap.get();
        //通过索引取出对象
        Object v = threadLocalMap.indexedVariable(index);
        if (v != InternalThreadLocalMap.UNSET) {
            return (V) v;
        }
        //初始化
        return initialize(threadLocalMap);
    }
}
```

# Recycler(对象池)

轻量级对象池

```java
public class RecycleTest {
    private static final Recycler<User> RECYCLER = new Recycler<User>() {
        @Override
        protected User newObject(Handle<User> handle) {
            return new User(handle);
        }
    };

    public static void main(String[] args) {
        User user = RECYCLER.get();
        System.out.println(user);
        user.recycle();
        User user1 = RECYCLER.get();
        System.out.println(user1);

    }

    private static class User {
        private final Recycler.Handle<User> handle;

        public User(Recycler.Handle<User> handle) {
            this.handle = handle;
        }

        public void recycle() {
            handle.recycle(this);
        }
    }
}
```

不回收拿到对象不一样,回收了就拿到一样的对象. 实际上维护了一个线程私有的对象栈,对象复用

```java
class Recycler {
    public final T get() {
        if (maxCapacityPerThread == 0) {
            return newObject((Handle<T>) NOOP_HANDLE);
        }
        Stack<T> stack = threadLocal.get();
        //从一个栈拿
        DefaultHandle<T> handle = stack.pop();
        if (handle == null) {
            handle = stack.newHandle();
            handle.value = newObject(handle);
        }
        return (T) handle.value;
    }

    @Deprecated
    public final boolean recycle(T o, Handle<T> handle) {
        if (handle == NOOP_HANDLE) {
            return false;
        }

        DefaultHandle<T> h = (DefaultHandle<T>) handle;
        if (h.stack.parent != this) {
            return false;
        }

        h.recycle(o);
        return true;
    }

    @Override
    public void recycle(Object object) {
        if (object != value) {
            throw new IllegalArgumentException("object does not belong to handle");
        }

        Stack<?> stack = this.stack;
        if (lastRecycledId != recycleId || stack == null) {
            throw new IllegalStateException("recycled already");
        }
        //把回收的对象压回栈内
        stack.push(this);
    }
}
```